import UIKit
import RHPlaceholder
import Kingfisher

class SearchCardTableViewCell: UITableViewCell {
    
    // Gradient
    
    let gradient = CAGradientLayer()
    var gradientSet = [[CGColor]]()
    var currentGradient: Int = 0
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var specialistLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel! {
        didSet {
            ratingLabel.cornerRadius(radius: 4.0)
        }
    }
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.circleCornerRadius()
        }
    }
    private var placeholderMarker = Placeholder(layerAnimator: InstaLayerAnimatorGradient.self)
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        backView.cornerRadius()

        enablePlaceHolder()
    }
    
    func configure(title: String,
                   imageURL: URL,
                   fullName: String,
                   color: UIColor?,
                   description: String) {
        nameLabel.text = fullName
        specialistLabel.text = title
        priceLabel.text = "3 $"
        profileImageView.kf.setImage(with: imageURL)
        ratingLabel.text = description
        backView.backgroundColor = color ?? .white
        
        disablePlaceHolder()
    }
    
    private func enablePlaceHolder() {
        placeholderMarker.register([backView,
                                    nameLabel,
                                    specialistLabel,
                                    priceLabel])
        placeholderMarker.startAnimation()
    }
    
    private func disablePlaceHolder() {
        placeholderMarker.remove()
    }
    
}

extension SearchCardTableViewCell {
    
    private func configureAnimationGradient() {
        for _ in 0...7 {
            gradientSet.append([UIColor.random.cgColor, UIColor.random.cgColor])
        }
        
        gradient.frame = self.backView.bounds
        gradient.colors = gradientSet[currentGradient]
        gradient.startPoint = CGPoint(x:0, y:0)
        gradient.endPoint = CGPoint(x:1, y:1)
        gradient.zPosition = -1.0
        gradient.drawsAsynchronously = true
        self.backView.layer.addSublayer(gradient)
        
        animateGradient()
    }
    
    private func animateGradient() {
        if currentGradient < gradientSet.count - 1 {
            currentGradient += 1
        } else {
            currentGradient = 0
        }
        
        let gradientChangeAnimation = CABasicAnimation(keyPath: "colors")
        gradientChangeAnimation.duration = 5.0
        gradientChangeAnimation.toValue = gradientSet[currentGradient]
        gradientChangeAnimation.fillMode = CAMediaTimingFillMode.forwards
        gradientChangeAnimation.isRemovedOnCompletion = false
        gradientChangeAnimation.delegate = self
        gradient.add(gradientChangeAnimation, forKey: "colorChange")
    }
    
}

extension SearchCardTableViewCell: CAAnimationDelegate {
    func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
        if flag {
            gradient.colors = gradientSet[currentGradient]
            animateGradient()
        }
    }
}

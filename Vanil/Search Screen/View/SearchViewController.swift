import UIKit

class SearchViewController: BaseViewController<SearchViewModel> {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            searchTextField.placeholder = L10n.Search.Screen.Textfield.search
        }
    }
    @IBOutlet weak var resultsForLabel: UILabel! {
        didSet {
            resultsForLabel.text = String(format: "%i %@ %@", 22, L10n.Search.Screen.Label.resultsfor, "Учитель английского")
        }
    }
    @IBOutlet weak var searchTableView: UITableView! {
        didSet {
            searchTableView.delegate = self
            searchTableView.dataSource = self
            searchTableView.separatorStyle = .none
        }
    }
    
    // Gradient
    let gradient = CAGradientLayer()
    var gradientSet = [[CGColor]]()
    var currentGradient: Int = 0
    
    override func loadView() {
        super.loadView()

        configureSearchTextField()
        configureAnimationGradient()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    private func configureSearchTextField() {
        searchTextField.cornerRadius()
        
        let searchImageView = UIImageView()
        searchImageView.contentMode = .scaleAspectFit
        searchImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        searchImageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        searchImageView.image = Asset.search.image
        
        let searchFlexibleSpaceView1 = UIView()
        searchFlexibleSpaceView1.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        let searchFlexibleSpaceView2 = UIView()
        searchFlexibleSpaceView2.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        let searchStackView = UIStackView()
        searchStackView.addArrangedSubview(searchFlexibleSpaceView1)
        searchStackView.addArrangedSubview(searchImageView)
        searchStackView.addArrangedSubview(searchFlexibleSpaceView2)
        
        searchTextField.leftView = searchStackView
        searchTextField.leftViewMode = .always
    }
}

extension SearchViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchCard") as! SearchCardTableViewCell
        return cell
    }
    
}

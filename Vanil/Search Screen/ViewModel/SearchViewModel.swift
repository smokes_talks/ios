import Foundation
import VanilKit

class SearchViewModel: ViewModel {
    
    let searchInfo: SearchModel?
    
    init(searchInfo: SearchModel?) {
        self.searchInfo = searchInfo
    }
    
}

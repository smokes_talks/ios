import UIKit
import MapKit

class Restaurant: NSObject, MKAnnotation {
  let title: String?
  let coordinate: CLLocationCoordinate2D

  init(
    title: String?,
    coordinate: CLLocationCoordinate2D
  ) {
    self.title = title
    self.coordinate = coordinate

    super.init()
  }
}


class MapViewController: BaseViewController<MapViewModel> {

    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let initialLocation = CLLocation(latitude: 46.4873195, longitude: 30.7392776)
        mapView.centerToLocation(initialLocation)
        
        let restaurant = Restaurant(
          title: "Vanila",
          coordinate: CLLocationCoordinate2D(latitude: 46.4873195, longitude: 30.7392776))
        mapView.addAnnotation(restaurant)
    }

    
    
}

private extension MKMapView {
  func centerToLocation(
    _ location: CLLocation,
    regionRadius: CLLocationDistance = 1000
  ) {
    let coordinateRegion = MKCoordinateRegion(
      center: location.coordinate,
      latitudinalMeters: regionRadius,
      longitudinalMeters: regionRadius)
    setRegion(coordinateRegion, animated: true)
  }
}

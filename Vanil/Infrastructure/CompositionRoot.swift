import UIKit
import VanilKit

class CompositionRoot {

    var pushTheLimitsKitCompositionRoot: VanilKitCompositionRoot {
        return VanilKitCompositionRoot.sharedInstance
    }

    static var sharedInstance: CompositionRoot = CompositionRoot()

    var rootTabBarController: UITabBarController!

    required init() {
        configureRootTabBarController()
    }

    private func configureRootTabBarController() {
        rootTabBarController = UITabBarController()
        rootTabBarController.tabBar.isTranslucent = false
        rootTabBarController.tabBar.tintColor = UIColor(red: 1, green: 0.8, blue: 0, alpha: 1)
        rootTabBarController.tabBar.barTintColor = UIColor(red: 0.086, green: 0.086, blue: 0.086, alpha: 0.94)

        var viewControllersList: [UIViewController] = [UIViewController]()
        
        
        let searchViewController = resolveSignInViewController()
        searchViewController.tabBarItem = UITabBarItem(title: "Меню", image: Asset.menu.image, tag: 1)
        searchViewController.tabBarItem.selectedImage = Asset.menu.image
        searchViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        viewControllersList.append(searchViewController)

//        let menuViewController = resolveMenuViewController()
//        menuViewController.tabBarItem = UITabBarItem(title: "Меню", image: Asset.menu.image, tag: 1)
//        menuViewController.tabBarItem.selectedImage = Asset.menu.image
//        menuViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
//        viewControllersList.append(menuViewController)

        let newsViewController = resolveNewsViewController()
        newsViewController.tabBarItem = UITabBarItem(title: "Новости", image: Asset.news.image, tag: 1)
        newsViewController.tabBarItem.selectedImage = Asset.news.image
        newsViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        viewControllersList.append(newsViewController)
        
        let bonusesViewController = resolveBonusesViewController()
        bonusesViewController.tabBarItem = UITabBarItem(title: "Бонусы", image: Asset.bonuses.image, tag: 1)
        bonusesViewController.tabBarItem.selectedImage = Asset.bonuses.image
        bonusesViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        viewControllersList.append(bonusesViewController)
        
        let profileViewController = resolveProfileViewController()
        profileViewController.tabBarItem = UITabBarItem(title: "Профиль", image: Asset.profile.image, tag: 1)
        profileViewController.tabBarItem.selectedImage = Asset.profile.image
        profileViewController.tabBarItem.imageInsets = UIEdgeInsets(top: 3, left: 0, bottom: 0, right: 0)
        viewControllersList.append(profileViewController)

        let viewControllers = viewControllersList.map { (viewController) -> UIViewController in
            let navigationController = UINavigationController(rootViewController: viewController)
            return navigationController
        }

        rootTabBarController.setViewControllers(viewControllers, animated: true)
    }
    
    func pushTabBarTo(index: Int) {
        rootTabBarController.selectedIndex = index
    }
    
    // MARK: ViewControllers
    
    func resolveIntroductionViewController() -> IntroductionViewController {
        let vc = IntroductionViewController.instantiateFromStoryboard("Introduction")
        vc.viewModel = resolveIntroductionViewModel()
        return vc
    }
    
    func resolveHomeViewController() -> HomeViewController {
        let vc = HomeViewController.instantiateFromStoryboard("Home")
        vc.viewModel = resolveHomeViewModel(view: vc)
        return vc
    }
    
    func resolveSearchViewController(_ searchInfo: SearchModel?) -> SearchViewController {
        let vc = SearchViewController.instantiateFromStoryboard("Search")
        vc.viewModel = resolveSearchViewModel(searchInfo: searchInfo)
        return vc
    }
    
//    func resolveProfileViewController(profile: ProfileModel?) -> ProfileViewController {
//        let vc = ProfileViewController.instantiateFromStoryboard("Profile")
//        vc.viewModel = resolveProfileViewModel(view: vc, profile: profile)
//        return vc
//    }
    
    func resolveMyProfileViewController() -> MyProfileViewController {
        let vc = MyProfileViewController.instantiateFromStoryboard("MyProfile")
        vc.viewModel = resolveMyProfileViewModel()
        return vc
    }
    
    func resolveSettingsViewController() -> SettingsViewController {
        let vc = SettingsViewController.instantiateFromStoryboard("Settings")
        vc.viewModel = resolveSettingsViewModel()
        return vc
    }
    
    func resolveGreetingViewController() -> GreetingViewController {
        let vc = GreetingViewController.instantiateFromStoryboard("Greeting")
        vc.viewModel = resolveGreetingViewModel()
        return vc
    }
    
    func resolveSplashScreenViewController() -> SplashScreenViewController {
        let vc = SplashScreenViewController.instantiateFromStoryboard("Splash")
        vc.viewModel = resolveSplashViewModel(view: vc)
        return vc
    }
    
    func resolveSignInViewController() -> SignInViewController {
        let vc = SignInViewController.instantiateFromStoryboard("SignIn")
        vc.viewModel = resolveSignInViewModel()
        return vc
    }
    
    func resolvePhoneNumberViewController() -> PhoneNumberViewController {
        let vc = PhoneNumberViewController.instantiateFromStoryboard("PhoneNumber")
        vc.viewModel = resolvePhoneNumberViewModel()
        return vc
    }

    func resolveAuthorizationViewController() -> AuthorizationViewController {
        let vc = AuthorizationViewController.instantiateFromStoryboard("Authorization")
        vc.viewModel = resolveAuthorizationViewModel()
        return vc
    }
    
    func resolvePersonalDataViewController() -> PersonalDataViewController {
        let vc = PersonalDataViewController.instantiateFromStoryboard("PersonalData")
        vc.viewModel = resolvePersonalDataViewModel()
        return vc
    }
    
    func resolveMenuViewController() -> MenuViewController {
        let vc = MenuViewController.instantiateFromStoryboard("Menu")
        vc.viewModel = resolveMenuViewModel(view: vc,
                                            cartDataStore: resolveCartDataStore())
        return vc
    }
    
    func resolveOrderViewController() -> OrderViewController {
        let vc = OrderViewController.instantiateFromStoryboard("Order")
        vc.viewModel = resolveOrderViewModel()
        return vc
    }
    
    func resolveQrScunViewController() -> QrScunViewController {
        let vc = QrScunViewController.instantiateFromStoryboard("QrScun")
        vc.viewModel = resolveQrScunViewModel()
        return vc
    }
    
    func resolveProfileViewController() -> ProfileViewController {
        let vc = ProfileViewController.instantiateFromStoryboard("Profile")
        vc.viewModel = resolveProfileViewModel()
        return vc
    }
    
    func resolveEditProfileViewController() -> EditProfileViewController {
        let vc = EditProfileViewController.instantiateFromStoryboard("EditProfile")
        vc.viewModel = resolveEditProfileViewModel()
        return vc
    }
    
    func resolveBonusesViewController() -> BonusesViewController {
        let vc = BonusesViewController.instantiateFromStoryboard("Bonuses")
        vc.viewModel = resolveBonusesViewModel()
        return vc
    }
    
    func resolveNewsViewController() -> NewsViewController {
        let vc = NewsViewController.instantiateFromStoryboard("News")
        vc.viewModel = resolveNewsViewModel()
        return vc
    }
    
    func resolveProgrammViewController() -> ProgrammViewController {
        let vc = ProgrammViewController.instantiateFromStoryboard("Programm")
        vc.viewModel = resolveProgrammViewModel()
        return vc
    }
  
    func resolveHistoryViewController() -> HistoryViewController {
        let vc = HistoryViewController.instantiateFromStoryboard("History")
        vc.viewModel = resolveHistoryViewModel()
        return vc
    }
    
//
//    func resolveContentViewController() -> ContentViewController {
//        let vc = ContentViewController.instantiateFromStoryboard("Content")
//        vc.viewModel = resolveContentViewModel()
//        return vc
//    }
//
//    func resolveRestaurantsViewController() -> RestaurantsViewController {
//        let vc = RestaurantsViewController.instantiateFromStoryboard("Restaurants")
//        vc.viewModel = resolveRestaurantsViewModel()
//        return vc
//    }
//
//    func resolveMapViewController() -> MapViewController {
//        let vc = MapViewController.instantiateFromStoryboard("Map")
//        vc.viewModel = resolveMapViewModel()
//        return vc
//    }
    
    func resolveSignUpViewController() -> SignUpViewController {
        let vc = SignUpViewController.instantiateFromStoryboard("SignUp")
        vc.viewModel = resolveSignUpViewModel()
        return vc
    }
    
//    func resolveProjectsViewController() -> ProjectsViewController {
//        let vc = ProjectsViewController.instantiateFromStoryboard("Projects")
//        vc.viewModel = resolveProjectsViewModel()
//        return vc
//    }
    
    func resolveForgotPasswordViewController() -> ForgotPasswordViewController {
        let vc = ForgotPasswordViewController.instantiateFromStoryboard("ForgotPassword")
        vc.viewModel = resolveForgotPasswordViewModel()
        return vc
    }
    
    func resolveNewPasswordViewController() -> NewPasswordViewController {
        let vc = NewPasswordViewController.instantiateFromStoryboard("NewPassword")
        vc.viewModel = resolveNewPasswordViewModel()
        return vc
    }
    
//    func resolveVerificationCodeViewController() -> VerificationCodeViewController {
//        let vc = VerificationCodeViewController.instantiateFromStoryboard("VerificationCode")
//        vc.viewModel = resolveVerificationCodeViewModel()
//        return vc
//    }
    
    func resolveNotificationsViewController() -> NotificationsViewController {
        let vc = NotificationsViewController.instantiateFromStoryboard("Notifications")
        vc.viewModel = resolveNotificationsViewModel()
        return vc
    }
    
    func resolveNotificationsSettingsViewController() -> NotificationsSettingsViewController {
        let vc = NotificationsSettingsViewController.instantiateFromStoryboard("NotificationsSettings")
        vc.viewModel = resolveNotificationsSettingsViewModel()
        return vc
    }
    
    func resolveProfileSettingsViewController() -> ProfileSettingsViewController {
        let vc = ProfileSettingsViewController.instantiateFromStoryboard("ProfileSettings")
        vc.viewModel = resolveProfileSettingsViewModel()
        return vc
    }
    
    func resolveSupportViewController() -> SupportViewController {
        let vc = SupportViewController.instantiateFromStoryboard("Support")
        vc.viewModel = resolveSupportViewModel()
        return vc
    }
    
//    func resolveProjectOverviewViewController(project: ProjectAPIModel) -> ProjectOverviewViewController {
//        let vc = ProjectOverviewViewController.instantiateFromStoryboard("ProjectOverview")
//        vc.viewModel = resolveProjectOverviewViewModel(projectToOverview: project)
//        return vc
//    }

    // MARK: ViewModels
    
    func resolveIntroductionViewModel() -> IntroductionViewModel {
        return IntroductionViewModel()
    }
    
    func resolveHomeViewModel(view: HomeView) -> HomeViewModel {
        return HomeViewModel(getBestSearchesProfiles: resolveGetPopularSearches(),
                             view: view)
    }
    
    func resolveSearchViewModel(searchInfo: SearchModel?) -> SearchViewModel {
        return SearchViewModel(searchInfo: searchInfo)
    }
    
//    func resolveProfileViewModel(view: ProfileView, profile: ProfileModel?) -> ProfileViewModel {
//        return ProfileViewModel(with: view, profile: profile)
//    }
    
    func resolveMyProfileViewModel() -> MyProfileViewModel {
        return MyProfileViewModel()
    }
    
    func resolveGreetingViewModel() -> GreetingViewModel {
        return GreetingViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolveSignInViewModel() -> SignInViewModel {
        return SignInViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolvePhoneNumberViewModel() -> PhoneNumberViewModel {
        return PhoneNumberViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolveAuthorizationViewModel() -> AuthorizationViewModel {
        return AuthorizationViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolvePersonalDataViewModel() -> PersonalDataViewModel {
        return PersonalDataViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolveMenuViewModel(view: MainView, cartDataStore: CartDataStore) -> MenuViewModel {
        return MenuViewModel(view: view,
                             authenticationAPIManager: resolveAuthenticationAPIManager(),
                             cartDataStore: cartDataStore)
    }
    
    func resolveOrderViewModel() -> OrderViewModel {
        return OrderViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolveQrScunViewModel() -> QrScunViewModel {
        return QrScunViewModel()
    }
    
    func resolveProfileViewModel() -> ProfileViewModel {
        return ProfileViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolveEditProfileViewModel() -> EditProfileViewModel {
        return EditProfileViewModel()
    }
    
    func resolveBonusesViewModel() -> BonusesViewModel {
        return BonusesViewModel()
    }
    
        func resolveNewsViewModel() -> NewsViewModel {
            return NewsViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
    }
    
    func resolveProgrammViewModel() -> ProgrammViewModel {
            return ProgrammViewModel()
    }
    
    func resolveHistoryViewModel() -> HistoryViewModel {
            return HistoryViewModel()
    }
    
//    func resolveContentViewModel() -> ContentViewModel {
//        return ContentViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
//    }
//
//    func resolveRestaurantsViewModel() -> RestaurantsViewModel {
//        return RestaurantsViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
//    }
//
//    func resolveMapViewModel() -> MapViewModel {
//        return MapViewModel(authenticationAPIManager: resolveAuthenticationAPIManager())
//    }
    
    func resolveSignUpViewModel() -> SignUpViewModel {
        return SignUpViewModel()
    }
    
    func resolveSettingsViewModel() -> SettingsViewModel {
        return SettingsViewModel()
    }
    
//    func resolveProjectsViewModel() -> ProjectsViewModel {
//        return ProjectsViewModel(projectAPIManager: resolveProjectAPIManager())
//    }
    
    func resolveForgotPasswordViewModel() -> ForgotPasswordViewModel {
        return ForgotPasswordViewModel()
    }
    
    func resolveNewPasswordViewModel() -> NewPasswordViewModel {
        return NewPasswordViewModel()
    }
    
//    func resolveVerificationCodeViewModel() -> VerificationCodeViewModel {
//        return VerificationCodeViewModel()
//    }
    
    func resolveSplashViewModel(view: SplashScreenView) -> SplashScreenViewModel {
        return SplashScreenViewModel(view: view, getAppVersionAndCheckIt: resolveGetAppVersionAndCheckIt())
    }
    
    func resolveNotificationsViewModel() -> NotificationsViewModel {
        return NotificationsViewModel()
    }
    
    func resolveNotificationsSettingsViewModel() -> NotificationsSettingsViewModel {
        return NotificationsSettingsViewModel()
    }
    
    func resolveProfileSettingsViewModel() -> ProfileSettingsViewModel {
        return ProfileSettingsViewModel()
    }
    
    func resolveSupportViewModel() -> SupportViewModel {
        return SupportViewModel()
    }
    
//    func resolveProjectOverviewViewModel(projectToOverview: ProjectAPIModel) -> ProjectOverviewViewModel {
//        return ProjectOverviewViewModel(projectToOverview: projectToOverview)
//    }

    func resolveRecoverFromLastState() -> RecoverFromLastState {
        return RecoverFromLastState()
    }
    
    func resolveGetAppVersionAndCheckIt() -> GetAppVersionAndCheckIt {
        return VanilKitCompositionRoot.sharedInstance.resolveGetAppVersionAndCheckIt
    }
    
    func resolveGetPopularSearches() -> GetBestSearchesProfiles {
        return VanilKitCompositionRoot.sharedInstance.resolveGetBestSearchesProfiles
    }
    
    func resolveProjectAPIManager() -> ProjectAPIManager {
        return VanilKitCompositionRoot.sharedInstance.resolveProjectAPIManager
    }
    
    func resolveAuthenticationAPIManager() -> AuthenticationAPIManager {
        return VanilKitCompositionRoot.sharedInstance.resolveAuthenticationAPIManager
    }
    
    func resolveCartDataStore() -> CartDataStore {
        return VanilKitCompositionRoot.sharedInstance.resolveCartDataStore
    }
}

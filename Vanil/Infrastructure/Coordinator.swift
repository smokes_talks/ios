import UIKit
import VanilKit

class Coordinator {

    static let shared = Coordinator()

    private var compositionRoot: CompositionRoot {
        return CompositionRoot.sharedInstance
    }
    
    private var tabBarController: UITabBarController {
        return compositionRoot.rootTabBarController
    }

    private var baseNavigationController: UINavigationController? {
        return lastPresentedViewController?.navigationController
    }

    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    var sceneDelegate: SceneDelegate {
        return UIApplication.shared.delegate as! SceneDelegate
    }

    private var lastPresentedViewController: UIViewController?

    private init() {}

    func showRootTabBarController() {
        let tabBarController = compositionRoot.rootTabBarController
        let navController = UINavigationController(rootViewController: compositionRoot.rootTabBarController)
        navController.navigationBar.isHidden = true
        tabBarController?.view.window?.rootViewController = navController
    }
    
//    func goToProfileVC(profile: ProfileModel?) {
//        push(compositionRoot.resolveProfileViewController(profile: profile))
//    }
    
    func goToPhoneNumber() {
        push(compositionRoot.resolvePhoneNumberViewController())
    }
    
    func goToAuthorization() {
        push(compositionRoot.resolveAuthorizationViewController())
    }
    
    func goToPersonalData() {
        push(compositionRoot.resolvePersonalDataViewController())
    }
    
    func goToMenu() {
        push(compositionRoot.resolveMenuViewController())
    }
    
    func goToOrder() {
        push(compositionRoot.resolveOrderViewController())
    }
    
    func goToEditProfile() {
        push(compositionRoot.resolveEditProfileViewController())
    }
    
    func goToProgramm() {
        push(compositionRoot.resolveProgrammViewController())
    }
    
    func goToHistory() {
        push(compositionRoot.resolveHistoryViewController())
    }
    
//    func goToNews() {
//        push(compositionRoot.resolveNewsViewController())
//    }
//
//    func goToMap() {
//        push(compositionRoot.resolveMapViewController())
//    }
    
    func goToSettingsVC() {
        push(compositionRoot.resolveSettingsViewController())
    }
    
    func goToSignInVC() {
        push(compositionRoot.resolveSignInViewController())
    }
    
    func goToSignUpVC() {
        push(compositionRoot.resolveSignUpViewController())
    }
    
    func goToSearchVC(_ searchInfo: SearchModel?) {
        push(compositionRoot.resolveSearchViewController(searchInfo))
    }
    
//    func goToProjectsVC() {
//        push(compositionRoot.resolveProjectsViewController())
//    }
    
    func goToForgotPasswordVC() {
        push(compositionRoot.resolveForgotPasswordViewController())
    }
    
    func goToNewPasswordVC() {
        push(compositionRoot.resolveNewPasswordViewController())
    }
    
//    func goToVerificationCodeVC() {
//        push(compositionRoot.resolveVerificationCodeViewController())
//    }
    
    func goToNotificationsVC() {
        push(compositionRoot.resolveNotificationsViewController())
    }
    
    func goToNotificationsSettingsVC() {
        push(compositionRoot.resolveNotificationsSettingsViewController())
    }
    
//    func goToProjectOverviewVC(project: ProjectAPIModel) {
//        push(compositionRoot.resolveProjectOverviewViewController(project: project))
//    }
    
    func goToProfileSettingsVC() {
        push(compositionRoot.resolveProfileSettingsViewController())
    }
    
    func goToSupportVC() {
        push(compositionRoot.resolveSupportViewController())
    }

    func push(_ vc: UIViewController) {
        if let baseNavigationController = baseNavigationController {
            baseNavigationController.pushViewController(vc, animated: true)
            lastPresentedViewController = vc
        } else {
            if let navigationController = tabBarController.selectedViewController as? UINavigationController {
                navigationController.pushViewController(vc, animated: true)
            }
        }
    }

}

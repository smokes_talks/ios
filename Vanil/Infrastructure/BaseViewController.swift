import UIKit

class BaseViewController<T>: UIViewController, BaseHandshakesUI where T: ViewModel {
    var observer = Observer()
    
    var viewModel: T!
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        // If we are inside a navigation bar, we hide it.
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)]
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.backgroundColor = .clear
        navigationController?.navigationBar.isTranslucent = true
        //navigationController?.navigationBar.tintColor = .black
        navigationController?.navigationBar.tintColor = UIColor(red: 1, green: 0.8, blue: 0, alpha: 1)
        
        // didEnterBackgroundNotification
        NotificationCenter.default.addObserver(self, selector: #selector(willEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(sender:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(sender:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        viewModel.viewDidLoad()
        setupTapGesture()
        
    }
    
    func setupTapGesture() {
//        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
//        tap.cancelsTouchesInView = false
//        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bindWithObserver()
        configureColors()
        viewModel.viewWillAppear()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.viewDidAppear()
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        viewModel.viewWillDisappear()
        observer.invalidateAll()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        viewModel.viewDidDisappear()
    }
    
    @objc func willEnterForeground() {
        viewModel.willEnterForeground()
    }
    
    @objc func didEnterBackground() {
        viewModel.didEnterBackground()
    }
    
    @objc func keyboardWillShow(sender: Notification) {
        
    }
    
    @objc func keyboardWillHide(sender: Notification) {
        
    }
    
    func bindWithObserver() {}
    
    func set(viewModel: T) {
        self.viewModel = viewModel
    }
    
    func configureColors() {
        //        self.tabBarController?.tabBar.barTintColor = Theme.current.mainColor
        //        self.tabBarController?.tabBar.tintColor = Theme.current.additionalColor
        //        self.navigationController?.navigationBar.barTintColor = Theme.current.mainColor
        //        self.navigationItem.titleView?.tintColor = Theme.current.additionalColor
    }
    
}

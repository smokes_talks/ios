import UIKit
import PhoneNumberKit
import FirebaseAuth
import FirebaseUI
import FirebaseAuth


typealias FIRUser = FirebaseAuth.User

class PhoneNumberViewController: BaseViewController<PhoneNumberViewModel> {
   
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet var keyboardView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var formatNumberImage: UIImageView!
    
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        phoneNumberTextField.becomeFirstResponder()
        configureBackground()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
//        FUIAuth.defaultAuthUI()?.delegate = self
//        let phoneProvider = FUIPhoneAuth.init(authUI: FUIAuth.defaultAuthUI()!)
//        FUIAuth.defaultAuthUI()?.providers = [phoneProvider]
//        phoneProvider.signIn(withPresenting: self, phoneNumber: nil)
        
        phoneNumberTextField.delegate = self
        phoneNumberTextField.keyboardType = .phonePad
        phoneNumberTextField.becomeFirstResponder()
        
        navigationController?.navigationBar.backIndicatorImage = UIImage(named: "arrow_back")
        navigationController?.navigationBar.tintColor = UIColor(red: 0.949, green: 0.788, blue: 0.298, alpha: 1)
        
        formatNumberImage.isHidden = true
        configureKeyboardView()
        configureButton()
        }
        
        func configureBackground() {
            let layer0 = CAGradientLayer()
            layer0.colors = [
              UIColor(red: 0.15, green: 0.168, blue: 0.183, alpha: 1).cgColor,
              UIColor(red: 0.085, green: 0.099, blue: 0.115, alpha: 1).cgColor
            ]
            layer0.locations = [0.03, 0.77]
            layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
            layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
            layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 1, c: -1, d: 0.21, tx: 0.5, ty: -0.11))
            layer0.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.5*view.bounds.size.height)
            layer0.position = view.center
            self.view.layer.insertSublayer(layer0, at: 0)
        }
    
    func configureButton() {
        let layer1 = CAGradientLayer()
        layer1.colors = [
          UIColor(red: 0.137, green: 0.145, blue: 0.149, alpha: 1).cgColor,
          UIColor(red: 0.255, green: 0.263, blue: 0.271, alpha: 1).cgColor
        ]
        layer1.locations = [0, 1]
        layer1.startPoint = CGPoint(x: 0.75, y: 0.5)
        layer1.endPoint = CGPoint(x: 0.25, y: 0.5)
        layer1.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.1*nextButton.bounds.size.height)
        layer1.position = view.center
        self.nextButton.layer.insertSublayer(layer1, at: 0)
        self.nextButton.bringSubviewToFront(self.nextButton.imageView!)
        self.nextButton.clipsToBounds = true
    }
    
    func configureKeyboardView() {
        phoneNumberTextField.inputAccessoryView = keyboardView
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        if (self.phoneNumberTextField.text!.count == 16) {
            let phoneNumber = phoneNumberTextField.text ?? ""
            PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
                if let error = error {
                    print(error as Any)
                    return
                }
                // Sign in using the verificationID and the code sent to the user
                // ...
                UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            }
            Coordinator.shared.goToAuthorization()
            formatNumberImage.isHidden = true
        } else {
            formatNumberImage.isHidden = false
        }
        
    }
}

extension PhoneNumberViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 16
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        formatNumberImage.isHidden = true
        textField.text = PartialFormatter().formatPartial(phoneNumberTextField.text!)
        
        return newString.length <= maxLength
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        formatNumberImage.isHidden = false
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        return true
    }
}

extension PhoneNumberViewController: FUIAuthDelegate {
    func authUI(_ authUI: FUIAuth, didSignInWith authDataResult: AuthDataResult?, error: Error?) {
        if let error = error {
            assertionFailure("Error signing in: \(error.localizedDescription)")
            return
        }

        print("handle user signup / login")
    }
}

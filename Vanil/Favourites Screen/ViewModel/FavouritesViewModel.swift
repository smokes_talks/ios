import Foundation
import VanilKit

class FavouritesViewModel: ViewModel {
    
    // MARK: - Private properties
    private let projectAPIManager: ProjectAPIManager
    
    // MARK: - Public properties
    var projects: [ProjectAPIModel]?
    
    // MARK: - Initializer
    init(projectAPIManager: ProjectAPIManager) {
        self.projectAPIManager = projectAPIManager
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        fetchProjects {
            print()
        }
    }
    
    // MARK: - Public methods
    func fetchProjects(completionHandler: @escaping () -> ()) {
        self.projectAPIManager.fetchProjects { (projects) in
            let filteredProjects = projects?.filter({ (project) -> Bool in
                guard let id = project.id else { return false }
                if UserDefaults.standard.bool(forKey: "favouriteID-\(id)") {
                    return true
                } else {
                    return false
                }
            })
            print(filteredProjects)
        }
    }
}

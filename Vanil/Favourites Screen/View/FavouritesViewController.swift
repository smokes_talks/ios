import UIKit

class FavouritesViewController:
    BaseViewController<FavouritesViewModel>,
    UICollectionViewDelegate,
    UICollectionViewDataSource {
    
    @IBOutlet weak var favouritesCollectionView: UICollectionView!
    
}

extension FavouritesViewController {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favouriteCell", for: indexPath)
        return cell
    }
    
}

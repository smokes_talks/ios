import Foundation
import VanilKit

struct Info {
    var imageInfo: UIImage
    var title: String
    var isSwitch: Bool
}

class ProfileViewModel: ViewModel {
    
   public let information: [Info] = [Info(imageInfo: UIImage(named: "user_profile")!, title: "Редактирование профиля", isSwitch: false),
                               Info(imageInfo: UIImage(named: "heart")!, title: "О программе", isSwitch: false),
                               Info(imageInfo: UIImage(named: "ring")!, title: "Push-уведомления", isSwitch: true),
                               Info(imageInfo: UIImage(named: "burger")!, title: "История заказов", isSwitch: false),
                               Info(imageInfo: UIImage(named: "exit")!, title: "Выход", isSwitch: false)]
    
    private let authenticationAPIManager: AuthenticationAPIManager
    
    init(authenticationAPIManager: AuthenticationAPIManager) {
        self.authenticationAPIManager = authenticationAPIManager
    }
    
    func authenticateUserWith(username: String,
                              andPassword password: String,
                              errorHandler: @escaping (String, String) -> Void,
                              completionHandler: @escaping (Bool) -> Void) {
        
        self.authenticationAPIManager.authenticateUser(username: username,
                                                           andPassword: password,
                                                           completionHandler:
            { (token) in
                if let tokenFin = token {
                    UserDefaults.standard.set(tokenFin, forKey: "accessToken")
                }
                completionHandler(true)
        }) { (errorStatus) in
            let message = AuthenticationAPIErrorStatus.messageForUserByStatusCode(statusCode: errorStatus)
            errorHandler(message!.title, message!.message)
            completionHandler(false)
        }
    }
}


import UIKit

class InfoTableViewCell: UITableViewCell {

    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        notificationSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.7)
        notificationSwitch.layer.opacity = 1
        notificationSwitch.layer.shadowRadius = 5
        notificationSwitch.layer.shadowOffset = CGSize(width: 0, height: 0)
        notificationSwitch.layer.shadowColor = UIColor(red: 0.98, green: 0.933, blue: 0.224, alpha: 1).cgColor
        //notificationSwitch.transform = CGAffineTransform(scaleX: , y: 22)
    }

    func configure(infoImage: UIImage, title: String, state: Bool) {
        self.infoImageView.image = infoImage
        self.titleLabel.text = title
        self.notificationSwitch.isHidden = !state
        
    }
    
    @IBAction func switcheTapped(_ sender: Any) {
        
    }
    
}

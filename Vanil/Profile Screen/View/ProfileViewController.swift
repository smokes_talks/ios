import UIKit
import Foundation
import FirebaseAuth

class ProfileViewController: BaseViewController<ProfileViewModel> {

    @IBOutlet weak var infoTableView: UITableView! {
        didSet {
            infoTableView.delegate = self
            infoTableView.dataSource = self
        }
    }
    
    let alert = UIAlertController(title: "Выход", message: "Вы точно хотите выйти из приложения?", preferredStyle: UIAlertController.Style.alert)
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureAlert()
        self.title = "Профиль"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        tabBarController?.tabBar.isHidden = false
    }
    

    func configureAlert() {
        
        alert.addAction(UIAlertAction(title: "Подтвердить", style: UIAlertAction.Style.default, handler: confirmHandler))
        alert.addAction(UIAlertAction(title: "Отменить", style: UIAlertAction.Style.default, handler: cancelHandler))
        alert.setBackgroundColor(color: UIColor(red: 0.2, green: 0.2, blue: 0.2, alpha: 1))
        alert.view.tintColor = UIColor(red: 1, green: 0.8, blue: 0, alpha: 1)
        
        let titleColor = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let titleAttrString = NSMutableAttributedString(string: "Выход", attributes: titleColor)
        let messageAttrString = NSMutableAttributedString(string: "Вы точно хотите выйти из приложения?", attributes: titleColor)
        
        alert.setValue(titleAttrString, forKey: "attributedTitle")
        alert.setValue(messageAttrString, forKey: "attributedMessage")
    }

    func confirmHandler(alert: UIAlertAction!) {
        let firebaseAuth = Auth.auth()
       do {
         try firebaseAuth.signOut()
       } catch let signOutError as NSError {
         print ("Error signing out: %@", signOutError)
       }
         
    }
    
    func cancelHandler(alert: UIAlertAction!) {
       
    }
    
}

extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.information.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "infoTableViewCell", for: indexPath) as! InfoTableViewCell
        cell.configure(infoImage: viewModel.information[indexPath.row].imageInfo, title: viewModel.information[indexPath.row].title, state: viewModel.information[indexPath.row].isSwitch)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            Coordinator.shared.goToEditProfile()
        case 1:
            Coordinator.shared.goToProgramm()
        case 3:
            Coordinator.shared.goToHistory()
        case 4:
           showAlert()
        default:
            return
        }
    }
    
    func showAlert() {
        self.present(alert, animated: true, completion: nil)
    }
    
}

extension UIAlertController {

    //Set background color of UIAlertController
    func setBackgroundColor(color: UIColor) {
        if let bgView = self.view.subviews.first, let groupView = bgView.subviews.first, let contentView = groupView.subviews.first {
            contentView.backgroundColor = color
        }
    }
}

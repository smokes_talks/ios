import UIKit
import VanilKit

class RecoverFromLastState {
    
    private let kIsAuthorized = "isAuthorized"
    private var isAuthorized: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kIsAuthorized)
        }
    }

    func getViewController() -> UIViewController {
        let compositionRoot = CompositionRoot.sharedInstance
        return compositionRoot.resolveSplashScreenViewController()
    }
}

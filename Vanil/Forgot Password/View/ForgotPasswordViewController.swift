import UIKit

class ForgotPasswordViewController: BaseViewController<ForgotPasswordViewModel> {
    
    // MARK: - IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var sendButton: UIButton! {
        didSet {
            sendButton.cornerRadius(radius: 4)
        }
    }
    
    @IBOutlet weak var emailTextField: UITextField! {
        didSet {
            emailTextField.delegate = self
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        self.hidingKeyboardSettings()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - IBActions
    
    @IBAction func sendButtonPressed() {
        goToVerificationCode()
    }
    
    // MARK: - Private methods
    private func goToVerificationCode() {
//        Coordinator.shared.goToVerificationCodeVC()
    }
    
    // MARK: - Keyboard notifications
    @objc func keyboardWillShow(notification: NSNotification) {
        let frame = calculateScrollViewHeightWith(notification: notification)
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom = frame
        scrollView.contentInset = contentInset
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let frame = calculateScrollViewHeightWith(notification: notification)
        var contentInset: UIEdgeInsets = self.scrollView.contentInset
        contentInset.bottom -= frame
        scrollView.contentInset = contentInset
    }
    
    func calculateScrollViewHeightWith(notification: NSNotification) -> CGFloat  {
        let userInfo = notification.userInfo!
        var keyboardFrame: CGRect = (userInfo[UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        return keyboardFrame.size.height
    }
    
}


// MARK: - UITextFieldDelegate
extension ForgotPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
}


import UIKit

class MyProfileViewController: BaseViewController<MyProfileViewModel> {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var profileImageView: UIImageView! {
        didSet {
            profileImageView.circleCornerRadius()
        }
    }
    @IBOutlet weak var nameTextField: UITextField! {
        didSet {
            nameTextField.cornerRadius()
            nameTextField.leftSpace()
        }
    }
    @IBOutlet weak var phoneTextField: UITextField! {
        didSet {
            phoneTextField.cornerRadius()
            phoneTextField.leftSpace()
        }
    }
    @IBOutlet weak var whoYouAreTextField: UITextField! {
        didSet {
            whoYouAreTextField.cornerRadius()
            whoYouAreTextField.leftSpace()
        }
    }
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.cornerRadius()
        }
    }
    @IBOutlet weak var colorCollectionView: UICollectionView! {
        didSet {
            colorCollectionView.delegate = self
            colorCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var colorCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var skillsCollectionView: UICollectionView! {
        didSet {
            skillsCollectionView.delegate = self
            skillsCollectionView.dataSource = self
        }
    }
    @IBOutlet weak var skillsCollectionViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var addSkillTextField: UITextField! {
        didSet {
            addSkillTextField.cornerRadius()
            addSkillTextField.leftSpace()
        }
    }
    @IBOutlet weak var addSkillButton: UIButton! {
        didSet {
            addSkillButton.cornerRadius()
        }
    }
    @IBOutlet weak var bottomViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var telegramAddButton: UIButton! {
        didSet {
            telegramAddButton.cornerRadius()
        }
    }
    
    private var skills: [String]? = []
    private var colors: [UIColor] = [UIColor(red:0.08, green:0.08, blue:0.13, alpha:1.00),
                                     UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.00),
                                     UIColor(red:0.75, green:0.32, blue:0.95, alpha:1.00),
                                     UIColor(red:0.41, green:0.47, blue:0.97, alpha:1.00),
                                     UIColor(red:1.00, green:0.81, blue:0.36, alpha:1.00),
                                     UIColor(red:0.95, green:0.60, blue:0.29, alpha:1.00),
                                     UIColor(red:0.00, green:0.52, blue:0.96, alpha:1.00),
                                     UIColor(red:0.00, green:0.77, blue:0.55, alpha:1.00),
                                     UIColor(red:1.00, green:0.39, blue:0.49, alpha:1.00)]
    
    // Gradient
    
    let gradient = CAGradientLayer()
    var gradientSet = [[CGColor]]()
    var currentGradient: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureAnimationGradient()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        colorCollectionViewHeightConstraint.constant = colorCollectionView.contentSize.height
        skillsCollectionViewHeightConstraint.constant = skillsCollectionView.contentSize.height
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func dismissKeyboard() {
        super.dismissKeyboard()
    }
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
        Coordinator.shared.goToSettingsVC()
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func addSkillButtonPressed(_ sender: Any) {
        if let text = addSkillTextField.text, !text.isEmpty {
            // remove spacing
            let textWoutSpacing = text.replacingOccurrences(of: " ", with: "")
            skills?.append(textWoutSpacing)
            addSkillTextField.text = nil
            skillsCollectionView.reloadData()
            addSkillTextField.becomeFirstResponder()
        }
    }
    
    @objc override func keyboardWillShow(sender: Notification) {
        guard let keyboardSize = sender.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        let keyboardHeight = keyboardSize.cgRectValue.height
        scrollView.contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0.0)
    }
    
    @objc override func keyboardWillHide(sender: Notification) {
        scrollView.contentInset = .zero
//        scrollView.scrollIndicatorInsets = .zero
    }
    
}

extension MyProfileViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if colorCollectionView == collectionView {
            return colors.count
        } else {
            return skills?.count ?? 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorIdentifier", for: indexPath) as! MyProfileColorCollectionViewCell
        //                                                          for: indexPath) as! MyProfileColorCollectionViewCell
//        if colorCollectionView == collectionView {
//
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "colorIdentifier",
//                                                          for: indexPath) as! MyProfileColorCollectionViewCell
//            cell.configure(with: colors[indexPath.row])
//            return cell
//        } else {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "skillsCell",
//                                                          for: indexPath) as! SkillsCollectionViewCell
//            cell.configure(skill: skills?[indexPath.row] ?? "No data")
//            return cell
//        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if colorCollectionView == collectionView {
            return CGSize(width: 32.0, height: 32.0)
        } else {
            if let skill = skills?[indexPath.row],
                let font = UIFont(name: "Ubuntu", size: 13.0) {
                // 16.0 - left + right width
                let width = skill.width(withConstrainedHeight: 33.0, font: font) + 16.0
                return CGSize(width: width, height: 31)
            } else {
                return CGSize(width: 179, height: 31)
            }
        }
    }
    
}

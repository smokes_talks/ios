import UIKit

class MyProfileColorCollectionViewCell: UICollectionViewCell {
    
    func configure(with color: UIColor) {
        contentView.circleCornerRadius()
        contentView.layer.borderWidth = 4.0
        contentView.layer.borderColor = UIColor.white.cgColor
        contentView.backgroundColor = color
    }
}

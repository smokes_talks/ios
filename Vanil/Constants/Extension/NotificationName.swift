import Foundation

enum NotificationName {
    static let appEnterInForeground = NSNotification.Name("appEnterInForeground")
}

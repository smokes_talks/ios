// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
import AppKit.NSImage
internal typealias AssetColorTypeAlias = NSColor
internal typealias AssetImageTypeAlias = NSImage
#elseif os(iOS) || os(tvOS) || os(watchOS)
import UIKit.UIImage
internal typealias AssetColorTypeAlias = UIColor
internal typealias AssetImageTypeAlias = UIImage
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
    internal static let menu = ImageAsset(name: "menu")
    internal static let bonuses = ImageAsset(name: "bonuses")
    internal static let news = ImageAsset(name: "news")
    internal static let snacks = ImageAsset(name: "snacks")
    internal static let salads = ImageAsset(name: "salads")
    internal static let grill = ImageAsset(name: "grill")
    internal static let hot = ImageAsset(name: "hot")
    internal static let desserts = ImageAsset(name: "desserts")
    internal static let drinks = ImageAsset(name: "drinks")
    internal static let profileSkype = ImageAsset(name: "profile_skype")
    internal static let profileTelegram = ImageAsset(name: "profile_telegram")
    internal static let profileViber = ImageAsset(name: "profile_viber")
    internal static let profileWhatsapp = ImageAsset(name: "profile_whatsapp")
    internal static let settingsFeedback = ImageAsset(name: "settings_feedback")
    internal static let settingsNotifications = ImageAsset(name: "settings_notifications")
    internal static let settingsPrivacyPolicy = ImageAsset(name: "settings_privacy_policy")
    internal static let settingsProfile = ImageAsset(name: "settings_profile")
    internal static let settingsRateOurApp = ImageAsset(name: "settings_rate_our_app")
    internal static let homeIdle = ImageAsset(name: "home-idle")
    internal static let home = ImageAsset(name: "home")
    internal static let profileIdle = ImageAsset(name: "profile-idle")
    internal static let profile = ImageAsset(name: "profile")
    internal static let searchIdle = ImageAsset(name: "search-idle")
    internal static let search = ImageAsset(name: "search")
    internal static let abcde = ImageAsset(name: "abcde")
    internal static let airplane = ImageAsset(name: "airplane")
    internal static let apartmentsSizes = ImageAsset(name: "apartmentsSizes")
    internal static let apartmentsYield = ImageAsset(name: "apartmentsYield")
    internal static let avatarDefault = ImageAsset(name: "avatar_default")
    internal static let busket = ImageAsset(name: "busket")
    internal static let cellSelected = ImageAsset(name: "cellSelected")
    internal static let cellUnselected = ImageAsset(name: "cellUnselected")
    internal static let cellBackground = ImageAsset(name: "cell_background")
    internal static let checkMark = ImageAsset(name: "checkMark")
    internal static let connectIllustration = ImageAsset(name: "connect-illustration")
    internal static let correct = ImageAsset(name: "correct")
    internal static let ella = ImageAsset(name: "ella")
    internal static let expandBlack = ImageAsset(name: "expand-black")
    internal static let eyeOff = ImageAsset(name: "eye-off")
    internal static let eye = ImageAsset(name: "eye")
    internal static let favouriteOff = ImageAsset(name: "favouriteOff")
    internal static let favouriteOn = ImageAsset(name: "favouriteOn")
    internal static let filter = ImageAsset(name: "filter")
    internal static let house1 = ImageAsset(name: "house1")
    internal static let house2 = ImageAsset(name: "house2")
    internal static let house3 = ImageAsset(name: "house3")
    internal static let house4 = ImageAsset(name: "house4")
    internal static let logo = ImageAsset(name: "logo")
    internal static let logoClear = ImageAsset(name: "logoClear")
    internal static let messengerNew = ImageAsset(name: "messenger_new")
    internal static let noNotifications = ImageAsset(name: "noNotifications")
    internal static let noResults = ImageAsset(name: "no_results")
    internal static let notification = ImageAsset(name: "notification")
    internal static let numberOfApartments = ImageAsset(name: "numberOfApartments")
    internal static let priceRange = ImageAsset(name: "priceRange")
    internal static let profileTabBar = ImageAsset(name: "profileTabBar")
    internal static let projectsTabBar = ImageAsset(name: "projectsTabBar")
    internal static let settings = ImageAsset(name: "settings")
    internal static let signInFacebook = ImageAsset(name: "sign_in_facebook")
    internal static let signInGoogle = ImageAsset(name: "sign_in_google")
    internal static let signInLogo = ImageAsset(name: "sign_in_logo")
    internal static let signInTwitter = ImageAsset(name: "sign_in_twitter")
    internal static let skype = ImageAsset(name: "skype")
    internal static let splashBackground = ImageAsset(name: "splash_background")
    internal static let supportTabBar = ImageAsset(name: "supportTabBar")
    internal static let telegramNew = ImageAsset(name: "telegram-new")
    internal static let viber = ImageAsset(name: "viber")
    internal static let videoPlay = ImageAsset(name: "videoPlay")
    internal static let whatsapp = ImageAsset(name: "whatsapp")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal struct ColorAsset {
    internal fileprivate(set) var name: String
    
    #if swift(>=3.2)
    @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
    internal var color: AssetColorTypeAlias {
        return AssetColorTypeAlias(asset: self)
    }
    #endif
}

internal extension AssetColorTypeAlias {
    #if swift(>=3.2)
    @available(iOS 11.0, tvOS 11.0, watchOS 4.0, OSX 10.13, *)
    convenience init!(asset: ColorAsset) {
        let bundle = Bundle(for: BundleToken.self)
        #if os(iOS) || os(tvOS)
        self.init(named: asset.name, in: bundle, compatibleWith: nil)
        #elseif os(OSX)
        self.init(named: asset.name, bundle: bundle)
        #elseif os(watchOS)
        self.init(named: asset.name)
        #endif
    }
    #endif
}

internal struct DataAsset {
    internal fileprivate(set) var name: String
    
    #if (os(iOS) || os(tvOS) || os(OSX)) && swift(>=3.2)
    @available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
    internal var data: NSDataAsset {
        return NSDataAsset(asset: self)
    }
    #endif
}

#if (os(iOS) || os(tvOS) || os(OSX)) && swift(>=3.2)
@available(iOS 9.0, tvOS 9.0, OSX 10.11, *)
internal extension NSDataAsset {
    convenience init!(asset: DataAsset) {
        let bundle = Bundle(for: BundleToken.self)
        self.init(name: asset.name, bundle: bundle)
    }
}
#endif

internal struct ImageAsset {
    internal fileprivate(set) var name: String
    
    internal var image: AssetImageTypeAlias {
        let bundle = Bundle(for: BundleToken.self)
        #if os(iOS) || os(tvOS)
        let image = AssetImageTypeAlias(named: name, in: bundle, compatibleWith: nil)
        #elseif os(OSX)
        let image = bundle.image(forResource: name)
        #elseif os(watchOS)
        let image = AssetImageTypeAlias(named: name)
        #endif
        guard let result = image else { fatalError("Unable to load image named \(name).") }
        return result
    }
}

internal extension AssetImageTypeAlias {
    @available(iOS 1.0, tvOS 1.0, watchOS 1.0, *)
    @available(OSX, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
    convenience init!(asset: ImageAsset) {
        #if os(iOS) || os(tvOS)
        let bundle = Bundle(for: BundleToken.self)
        self.init(named: asset.name, in: bundle, compatibleWith: nil)
        #elseif os(OSX) || os(watchOS)
        self.init(named: asset.name)
        #endif
    }
}

private final class BundleToken {}

// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(OSX)
  import AppKit
  internal enum ColorName { }
#elseif os(iOS) || os(tvOS) || os(watchOS)
  import UIKit
  internal enum ColorName { }
#endif

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// MARK: - Colors

// swiftlint:disable identifier_name line_length type_body_length
internal extension ColorName {
  /// 0x8e2de2ff (r: 142, g: 45, b: 226, a: 255)
  internal static let gradient11 = #colorLiteral(red: 0.5568628, green: 0.1764706, blue: 0.8862745, alpha: 1.0)
  /// 0x4a00e0ff (r: 74, g: 0, b: 224, a: 255)
  internal static let gradient12 = #colorLiteral(red: 0.2901961, green: 0.0, blue: 0.8784314, alpha: 1.0)
  /// 0x4776e6ff (r: 71, g: 118, b: 230, a: 255)
  internal static let gradient13 = #colorLiteral(red: 0.2784314, green: 0.4627451, blue: 0.9019608, alpha: 1.0)
  /// 0x8e54e9ff (r: 142, g: 84, b: 233, a: 255)
  internal static let gradient14 = #colorLiteral(red: 0.5568628, green: 0.32941177, blue: 0.9137255, alpha: 1.0)
  /// 0x4e54c8ff (r: 78, g: 84, b: 200, a: 255)
  internal static let gradient15 = #colorLiteral(red: 0.30588236, green: 0.32941177, blue: 0.78431374, alpha: 1.0)
  /// 0x8f94fbff (r: 143, g: 148, b: 251, a: 255)
  internal static let gradient16 = #colorLiteral(red: 0.56078434, green: 0.5803922, blue: 0.9843137, alpha: 1.0)
  /// 0x43cea2ff (r: 67, g: 206, b: 162, a: 255)
  internal static let gradient21 = #colorLiteral(red: 0.2627451, green: 0.80784315, blue: 0.63529414, alpha: 1.0)
  /// 0x185a9dff (r: 24, g: 90, b: 157, a: 255)
  internal static let gradient22 = #colorLiteral(red: 0.09411765, green: 0.3529412, blue: 0.6156863, alpha: 1.0)
  /// 0x185a9dff (r: 24, g: 90, b: 157, a: 255)
  internal static let gradient23 = #colorLiteral(red: 0.09411765, green: 0.3529412, blue: 0.6156863, alpha: 1.0)
  /// 0x185a9dff (r: 24, g: 90, b: 157, a: 255)
  internal static let gradient24 = #colorLiteral(red: 0.09411765, green: 0.3529412, blue: 0.6156863, alpha: 1.0)
  /// 0x185a9dff (r: 24, g: 90, b: 157, a: 255)
  internal static let gradient25 = #colorLiteral(red: 0.09411765, green: 0.3529412, blue: 0.6156863, alpha: 1.0)
  /// 0x185a9dff (r: 24, g: 90, b: 157, a: 255)
  internal static let gradient26 = #colorLiteral(red: 0.09411765, green: 0.3529412, blue: 0.6156863, alpha: 1.0)
  /// 0x185a9dff (r: 24, g: 90, b: 157, a: 255)
  internal static let gradient31 = #colorLiteral(red: 0.09411765, green: 0.3529412, blue: 0.6156863, alpha: 1.0)
  /// 0xdd2476ff (r: 221, g: 36, b: 118, a: 255)
  internal static let gradient32 = #colorLiteral(red: 0.8666667, green: 0.14117648, blue: 0.4627451, alpha: 1.0)
  /// 0xec008cff (r: 236, g: 0, b: 140, a: 255)
  internal static let gradient33 = #colorLiteral(red: 0.9254902, green: 0.0, blue: 0.54901963, alpha: 1.0)
  /// 0xfc6767ff (r: 252, g: 103, b: 103, a: 255)
  internal static let gradient34 = #colorLiteral(red: 0.9882353, green: 0.40392157, blue: 0.40392157, alpha: 1.0)
  /// 0xeb3349ff (r: 235, g: 51, b: 73, a: 255)
  internal static let gradient35 = #colorLiteral(red: 0.92156863, green: 0.2, blue: 0.28627452, alpha: 1.0)
  /// 0xf45c43ff (r: 244, g: 92, b: 67, a: 255)
  internal static let gradient36 = #colorLiteral(red: 0.95686275, green: 0.36078432, blue: 0.2627451, alpha: 1.0)
  /// 0xff512fff (r: 255, g: 81, b: 47, a: 255)
  internal static let gradient41 = #colorLiteral(red: 1.0, green: 0.31764707, blue: 0.18431373, alpha: 1.0)
  /// 0xf09819ff (r: 240, g: 152, b: 25, a: 255)
  internal static let gradient42 = #colorLiteral(red: 0.9411765, green: 0.59607846, blue: 0.09803922, alpha: 1.0)
  /// 0xe65c00ff (r: 230, g: 92, b: 0, a: 255)
  internal static let gradient43 = #colorLiteral(red: 0.9019608, green: 0.36078432, blue: 0.0, alpha: 1.0)
  /// 0xf9d423ff (r: 249, g: 212, b: 35, a: 255)
  internal static let gradient44 = #colorLiteral(red: 0.9764706, green: 0.83137256, blue: 0.13725491, alpha: 1.0)
  /// 0xf09819ff (r: 240, g: 152, b: 25, a: 255)
  internal static let gradient45 = #colorLiteral(red: 0.9411765, green: 0.59607846, blue: 0.09803922, alpha: 1.0)
  /// 0xedde5dff (r: 237, g: 222, b: 93, a: 255)
  internal static let gradient46 = #colorLiteral(red: 0.92941177, green: 0.87058824, blue: 0.3647059, alpha: 1.0)
  /// 0xff9966ff (r: 255, g: 153, b: 102, a: 255)
  internal static let gradient51 = #colorLiteral(red: 1.0, green: 0.6, blue: 0.4, alpha: 1.0)
  /// 0xff5e62ff (r: 255, g: 94, b: 98, a: 255)
  internal static let gradient52 = #colorLiteral(red: 1.0, green: 0.36862746, blue: 0.38431373, alpha: 1.0)
  /// 0xff5f6dff (r: 255, g: 95, b: 109, a: 255)
  internal static let gradient53 = #colorLiteral(red: 1.0, green: 0.37254903, blue: 0.42745098, alpha: 1.0)
  /// 0xffc371ff (r: 255, g: 195, b: 113, a: 255)
  internal static let gradient54 = #colorLiteral(red: 1.0, green: 0.7647059, blue: 0.44313726, alpha: 1.0)
  /// 0xff7e5fff (r: 255, g: 126, b: 95, a: 255)
  internal static let gradient55 = #colorLiteral(red: 1.0, green: 0.49411765, blue: 0.37254903, alpha: 1.0)
  /// 0xfeb47bff (r: 254, g: 180, b: 123, a: 255)
  internal static let gradient56 = #colorLiteral(red: 0.99607843, green: 0.7058824, blue: 0.48235294, alpha: 1.0)
}
// swiftlint:enable identifier_name line_length type_body_length

import UIKit

extension UITextField {

    func setInputViewDatePicker(target: Any, selector: Selector) {
        let screenWidth = UIScreen.main.bounds.width
        let datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 216))
        datePicker.datePickerMode = .date
        self.inputView = datePicker

        let toolBar = UIToolbar(frame: CGRect(x: 0.0, y: 0.0, width: screenWidth, height: 44.0))
        let flexible = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancel = UIBarButtonItem(title: "Cancel", style: .plain, target: nil, action: #selector(tapCancel))
        let barButton = UIBarButtonItem(title: "Done", style: .plain, target: target, action: selector)
        toolBar.setItems([cancel, flexible, barButton], animated: false)
        self.inputAccessoryView = toolBar
    }

    func clearInputViewDatePicker() {
        self.inputView = nil
        self.inputAccessoryView = nil
    }

    @objc func tapCancel() {
        self.resignFirstResponder()
    }

    // MARK: - Verification Code

    func createNonActivatedTextField() {
        self.keyboardType = .numberPad
        self.clipsToBounds = true
        self.textAlignment = .center
        self.layer.borderWidth = 1.5
        self.layer.masksToBounds = true
        deactivateTextField()
    }

    func deactivateTextField() {
        self.layer.borderColor = #colorLiteral(red: 0.9332318306, green: 0.9333917499, blue: 0.933221817, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 0.9685223699, green: 0.9686879516, blue: 0.9685119987, alpha: 1)
        self.textColor = #colorLiteral(red: 0.6077751517, green: 0.6078823209, blue: 0.6077683568, alpha: 1)
        self.placeholder = "•"
    }

    func activateTextField() {
        self.layer.borderColor = #colorLiteral(red: 0.4330122173, green: 0.642934978, blue: 0.4272718132, alpha: 1)
        self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        self.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        if self.placeholder == "•" {
            self.placeholder = ""
        }
    }
    
    func leftSpace() {
           let searchFlexibleSpaceView = UIView()
           searchFlexibleSpaceView.widthAnchor.constraint(equalToConstant: 10).isActive = true
           let searchStackView = UIStackView()
           searchStackView.addArrangedSubview(searchFlexibleSpaceView)
           leftView = searchStackView
           leftViewMode = .always
       }
    
}

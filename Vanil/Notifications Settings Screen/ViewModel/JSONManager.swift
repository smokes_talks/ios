import Foundation

struct UserSettings: Decodable {
    
    var text: String? = nil
    var setting: Bool? = nil
  
    func encode(_ dictionary: [String: Any]) -> String? {
        guard JSONSerialization.isValidJSONObject(dictionary) else {
            assertionFailure("Invalid json object received.")
            return nil
        }

        let jsonObject: NSMutableDictionary = NSMutableDictionary()
        let jsonData: Data

        dictionary.forEach { (arg) in
            jsonObject.setValue(arg.value, forKey: arg.key)
        }

        do {
            jsonData = try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        } catch {
            assertionFailure("JSON data creation failed with error: \(error).")
            return nil
        }

        guard let jsonString = String.init(data: jsonData, encoding: String.Encoding.utf8) else {
            assertionFailure("JSON string creation failed.")
            return nil
        }

        print("JSON string: \(jsonString)")
        return jsonString
    }
    
    func decode(text: String) -> [String: Any]? {
        if let json = text.data(using: .utf8) {
            do {
                let decoder = JSONDecoder()
                let product = try decoder.decode(UserSettings.self, from: json)
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}

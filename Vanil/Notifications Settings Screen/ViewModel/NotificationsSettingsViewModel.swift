import UIKit

class NotificationsSettingsViewModel: ViewModel {
    
    let headersArray = ["Sale", "Rent", "System", "Meeting"]
    
    let textsArray =
        [
            ["What new Offer is created?",
             "When Reservation  is confirmed for the Offer",
             "When offer is not longer valid",
             "When reservation is finished and selling phase begins",
             "When selling process is finished!",
             "When new image gallery is posted for the project"],
            
            ["When new rent payment was done"],
            
            ["prospects.show_user_welcome"],
            
            ["When new meeting is created",
             "When meeting starts",
             "When meeting ends",
             "When meeting is rescheduled",
             "When offer was created in the meeting"]
    ]
    
    var settingStatementsArray =
        [
            [true,
             true,
             true,
             true,
             true,
             true],
            
            [true],
            
            [true],
            
            [true,
             true,
             true,
             true,
             true]
    ]
    
}

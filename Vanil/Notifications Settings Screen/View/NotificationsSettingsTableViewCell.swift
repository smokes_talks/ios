import UIKit

protocol NotificationsSettingsTableViewCellDelegate {
    func switchValueChanged(by indexPath: IndexPath)
}

class NotificationsSettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var notificationLabel: UILabel!
    @IBOutlet weak var notificationSwitch: UISwitch!
    
    private var indexPath: IndexPath!
    var delegate: NotificationsSettingsTableViewCellDelegate!
    
    func configureCellWith(indexPath: IndexPath, delegate: NotificationsSettingsTableViewCellDelegate, settingText: String, statement: Bool) {
        self.indexPath = indexPath
        self.delegate = delegate
        notificationLabel.text = settingText
        notificationSwitch.isOn = statement
    }
    
    @IBAction func switchStatementChanged(_ sender: UISwitch) {
        delegate.switchValueChanged(by: indexPath)
        print("[\(indexPath.section)][\(indexPath.row)]")
    }
    
}

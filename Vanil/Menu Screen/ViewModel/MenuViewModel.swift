import Foundation
import VanilKit

protocol MainView {
    func cartCountUpdate(count: Int)
}

class MenuViewModel: ViewModel, CartDataStoreDelegate {
    
    let degree: [String] = ["Rare", "Medium", "Well"]
    let categories: [String] = ["Закуски", "Салаты", "Гриль", "Горячее", "Десерты", "Напитки"]
    
    let view: MainView
    private let authenticationAPIManager: AuthenticationAPIManager
    
    var cartDataStore: CartDataStore
    
    init(view: MainView,
         authenticationAPIManager: AuthenticationAPIManager,
         cartDataStore: CartDataStore) {
        self.authenticationAPIManager = authenticationAPIManager
        self.cartDataStore = cartDataStore
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cartDataStore.delegate = self
    }
    
    func authenticateUserWith(username: String,
                              andPassword password: String,
                              errorHandler: @escaping (String, String) -> Void,
                              completionHandler: @escaping (Bool) -> Void) {
        
        self.authenticationAPIManager.authenticateUser(username: username,
                                                       andPassword: password,
                                                           completionHandler:
            { (token) in
                if let tokenFin = token {
                    UserDefaults.standard.set(tokenFin, forKey: "accessToken")
                }
                completionHandler(true)
        }) { (errorStatus) in
            let message = AuthenticationAPIErrorStatus.messageForUserByStatusCode(statusCode: errorStatus)
            errorHandler(message!.title, message!.message)
            completionHandler(false)
        }
    }
    
    func addFoodToCart(indexPath: IndexPath, count: Int) {
        cartDataStore.put(indexPath.row, foodID: "test_food_id", count: count)
    }
    
    func removeFoodFromCart(indexPath: IndexPath) {
        cartDataStore.remove(indexPath.row, foodID: "test_food_id")
    }
    
    // MARK: - CartDataStoreDelegate
    
    func updatedStore(count: Int) {
        self.view.cartCountUpdate(count: count)
    }
    
}

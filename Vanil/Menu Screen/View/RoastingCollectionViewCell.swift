import UIKit

class RoastingCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var degreeOfRoastingLabel: UILabel!
    
    func configure(degeree: String) {
        self.degreeOfRoastingLabel.text = degeree
    }
    
    func toggleSelected () {
        if isSelected {
            degreeOfRoastingLabel.textColor = UIColor(red: 1, green: 0.8, blue: 0, alpha: 1)
            degreeOfRoastingLabel.font = UIFont(name: "Ubuntu-Bold", size: 13)
        } else {
            degreeOfRoastingLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
            degreeOfRoastingLabel.font = UIFont(name: "Ubuntu-Regular", size: 13)
        }
    }
    
}


import UIKit

class CategoriesCollectionViewCell: UICollectionViewCell {
    

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelHeightConstraint: NSLayoutConstraint!
    
    func configure(category: String) {
        self.titleLabel.text = category
        rotateLabel()
    }
    
    private func rotateLabel() {
        titleLabel.transform = CGAffineTransform(rotationAngle: -CGFloat.pi / 2)
        titleLabelHeightConstraint.constant = 144 
    }
    
    func toggleSelected() {
        if isSelected {
            titleLabel.textColor = UIColor(red: 1, green: 0.8, blue: 0, alpha: 1)
            titleLabel.font = UIFont(name: "Ubuntu-Bold", size: 13)
        } else {
            titleLabel.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.7)
            titleLabel.font = UIFont(name: "Ubuntu-Regular", size: 13)
        }
    }
}

import UIKit

class MenuViewController: BaseViewController<MenuViewModel>, MainView {
    
    @IBOutlet weak var menuTableView: UITableView! {
        didSet {
            menuTableView.delegate = self
            menuTableView.dataSource = self
            menuTableView.rowHeight = UITableView.automaticDimension
            menuTableView.estimatedRowHeight = 78
        }
    }
    
    @IBOutlet weak var subCategoriesCollectionView: UICollectionView! {
        didSet {
            subCategoriesCollectionView.delegate = self
            subCategoriesCollectionView.dataSource = self
        }
    }
    
    @IBOutlet weak var categoriesCollectionView: UICollectionView! {
        didSet {
            categoriesCollectionView.delegate = self
            categoriesCollectionView.dataSource = self
        }
    }
    
    let orderButton = UIButton()
    let costView = UIView()
    let costLabel = UILabel()
    var cartCount = Int()
    
    // MARK: - Lifecycle
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        tabBarController?.tabBar.isHidden = false
        if cartCount > 0 {
            orderButton.isHidden = false
        }

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        orderButton.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Меню"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        configureCostView()
        configureButton()
    }
    
    func configureButton() {
        orderButton.isHidden = true
        orderButton.frame = CGRect(x: (navigationController?.navigationBar.frame.width)! - 46 , y: (navigationController?.navigationBar.frame.height)!-50, width: 30, height: 30)
        orderButton.setImage(UIImage(named: "take_out"), for: .normal)
        orderButton.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        navigationController?.navigationBar.addSubview(orderButton)
    }
    
    func configureCostView() {
        costLabel.text = "155 грн"
        costLabel.textColor = .white
        costLabel.font = .italicSystemFont(ofSize: 9)
        costLabel.frame = CGRect(x: 4, y: 0, width: 36, height: 12)
        costView.frame = CGRect(x: -8, y: 26, width: 44, height: 12)
        costView.backgroundColor = UIColor(red: 0.913, green: 0.183, blue: 0.281, alpha: 1)
        costView.cornerRadius(radius: 6)
        orderButton.addSubview(costView)
        costView.addSubview(costLabel)
    }
    
    @objc func buttonAction(sender: UIButton!) {
        print("Button tapped")
        //orderButton.isHidden = true
        Coordinator.shared.goToOrder()
    }
    
    // MARK: - MainView
    
    func cartCountUpdate(count: Int) {
        cartCount = count
        if count <= 0 {
            orderButton.isHidden = true
        } else {
            orderButton.isHidden = false
        }
    }
    
}

extension MenuViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == subCategoriesCollectionView {
            return 3
        } else {
            return 6
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == subCategoriesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "roastingCollectionViewCell", for: indexPath) as! RoastingCollectionViewCell
            cell.configure(degeree: viewModel.degree[indexPath.row])
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesCollectionViewCell", for: indexPath) as! CategoriesCollectionViewCell
            cell.configure(category: viewModel.categories[indexPath.row])
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == subCategoriesCollectionView {
            let cell = collectionView.cellForItem(at: indexPath as IndexPath) as! RoastingCollectionViewCell
            cell.toggleSelected()
        } else {
            let cell = collectionView.cellForItem(at: indexPath as IndexPath) as! CategoriesCollectionViewCell
            cell.toggleSelected()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == subCategoriesCollectionView {
            let cell = collectionView.cellForItem(at: indexPath as IndexPath) as! RoastingCollectionViewCell
            cell.toggleSelected()
        } else {
            let cell = collectionView.cellForItem(at: indexPath as IndexPath) as! CategoriesCollectionViewCell
            cell.toggleSelected()
        }
    }
    
}

extension MenuViewController: UITableViewDelegate, UITableViewDataSource, MenuTableViewCellDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "menuTableViewCell", for: indexPath) as! MenuTableViewCell
        cell.configure(delegate: self, indexPath: indexPath)
        return cell
    }
    
    func addButtonPressed(indexPath: IndexPath) {
        menuTableView.reloadData()
        viewModel.addFoodToCart(indexPath: indexPath, count: 1)
    }
    
    func stepperValueChanged(indexPath: IndexPath, count: Int) {
        menuTableView.reloadData()
        viewModel.addFoodToCart(indexPath: indexPath, count: count)
    }
}


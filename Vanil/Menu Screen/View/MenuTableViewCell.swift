import UIKit

protocol MenuTableViewCellDelegate: class {
    func addButtonPressed(indexPath: IndexPath)
    func stepperValueChanged(indexPath: IndexPath, count: Int)
}

class MenuTableViewCell: UITableViewCell {
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var stepper: UIStepper!
    
    @IBOutlet weak var imageViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var priceToSafeAreaConstraint: NSLayoutConstraint!
    
    weak var delegate: MenuTableViewCellDelegate?
    var indexPath: IndexPath?
    
    var countValue: Int = 1 {
        didSet {
            if countValue <= 0 {
                showAddButton()
                hideStepper()
                configureStepper()
            } else {
                countLabel.text = "\(countValue)x"
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        hideStepper()
        countLabel.isHidden = true
        
        selectionStyle = .none
        configureStepper()
    }
    
    func configure(delegate: MenuTableViewCellDelegate, indexPath: IndexPath) {
        self.delegate = delegate
        self.indexPath = indexPath
    }
    
    @objc func stepperValueChanged(_ stepper: UIStepper) {
        if let indexPath = indexPath {
            self.delegate?.stepperValueChanged(indexPath: indexPath,
                                               count: Int(stepper.value))
        }
        countValue = Int(stepper.value)
    }
    
    @IBAction func addButtonPressed(_ sender: UIButton) {
        if let indexPath = indexPath {
            self.delegate?.addButtonPressed(indexPath: indexPath)
        }
        hideAddButton()
        showStepper()
    }
    
    private func showAddButton() {
        addButton.isHidden = false
        countLabel.isHidden = true
        weightLabel.textAlignment = .center
        priceToSafeAreaConstraint.priority = UILayoutPriority(rawValue: 750)
    }
    
    private func hideAddButton() {
        addButton.isHidden = true
        countLabel.isHidden = false
        weightLabel.textAlignment = .right
        priceToSafeAreaConstraint.priority = UILayoutPriority(rawValue: 1000)
    }
    
    private func showStepper() {
        stepper.isHidden = false
        imageViewBottomConstraint.priority = UILayoutPriority(rawValue: 750)
    }
    
    private func hideStepper() {
        stepper.isHidden = true
        imageViewBottomConstraint.priority = UILayoutPriority(rawValue: 1000)
    }
    
    private func configureStepper() {
        stepper.cornerRadius()
        stepper.isContinuous = false
        stepper.autorepeat = false
        stepper.wraps = false
        stepper.minimumValue = 0
        stepper.maximumValue = 20
        stepper.value = 1
        stepper.backgroundColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        stepper.setDecrementImage(stepper.decrementImage(for: .normal), for: .normal)
        stepper.setIncrementImage(stepper.incrementImage(for: .normal), for: .normal)
        stepper.tintColor = .white
        stepper.addTarget(self, action: #selector(stepperValueChanged(_:)), for: .valueChanged)
    }
    
}



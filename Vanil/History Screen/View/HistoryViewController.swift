import UIKit

class HistoryViewController: BaseViewController<HistoryViewModel> {

    let titleLabel = UILabel()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        titleLabel.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTitle()
        tabBarController?.tabBar.isHidden = true
    }
    
    func configureTitle() {
        titleLabel.text = "История заказов"
        titleLabel.font = .boldSystemFont(ofSize: 18)
        titleLabel.textColor = .white
        titleLabel.frame = CGRect(x: (navigationController?.navigationBar.frame.width)!/2 - 152/2 , y: (navigationController?.navigationBar.frame.height)!-44, width: 152, height: 41)
        navigationController?.navigationBar.addSubview(titleLabel)
    }
    

}

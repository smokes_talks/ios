import UIKit
import SceneKit
import VanilKit

class HomeViewController: BaseViewController<HomeViewModel>, HomeView {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.text = L10n.Main.Screen.Label.main
        }
    }
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            searchTextField.placeholder = L10n.Main.Screen.Textfield.search
            searchTextField.delegate = self
        }
    }
    @IBOutlet weak var searchTableView: UITableView! {
        didSet {
            searchTableView.delegate = self
            searchTableView.dataSource = self
            searchTableView.cornerRadius()
        }
    }
    @IBOutlet weak var candidatesTableView: UITableView! {
        didSet {
            candidatesTableView.delegate = self
            candidatesTableView.dataSource = self
            candidatesTableView.separatorStyle = .none
        }
    }
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTopLocationConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTopSuperViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewTopPopularLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var popularSearchesTopSearchBarConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationLabelButton: UIButton!
    @IBOutlet weak var popularSearchesCollectionView: UICollectionView! {
        didSet {
            popularSearchesCollectionView.delegate = self
            popularSearchesCollectionView.dataSource = self
        }
    }
    
    private let similarSearchTitles: [String] = ["UI/UX Designer", "Deputy", "President", "Housewife", "Dog walks", "Wash the dishes", "Eurovision Winner 2019"]
    private let similarSearchResultsCounts: [Int] = [25, 3, 1, 10, 6, 10, 2]
    private var skills: [String]? = ["Best UI/UX Designers", "10 Deputy in Odessa", "10 IT Agencies", "Best Housewifes in Odessa", "Dog walkers", "Wash the dishes", "Eurovision Winner 2019"]
    private var popularSearchSelectedIndexPath: IndexPath = IndexPath(row: 0, section: 0)
    
    private var isCanditatesOnTopPosition: Bool = false
    private var isLocationSearch: Bool = false {
        didSet {
            if isLocationSearch == true {
                searchTextField.placeholder = L10n.Main.Screen.Textfield.locationsearch
            } else {
                searchTextField.placeholder = L10n.Main.Screen.Textfield.search
            }
            searchTableView.reloadData()
        }
    }
    private var locationSearch: String? {
        didSet {
            if let locationSearch = locationSearch {
                if !locationSearch.isEmpty  {
                    viewModel.searchPlaces(text: locationSearch)
                } else {
                    viewModel.searchPlaces = []
                }
            }
        }
    }
    
    // Gradient
    
    let gradient = CAGradientLayer()
    var gradientSet = [[CGColor]]()
    var currentGradient: Int = 0
    
    override func loadView() {
        super.loadView()
        
        configureSearchTextField()
        configureAnimationGradient()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Menu"
        let tap = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        backgroundView.addGestureRecognizer(tap)
        
        searchTextField.addTarget(self, action: #selector(textFieldDidChange(_:)),
                                  for: .editingChanged)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        searchTableView.reloadData()

        updateTableViewHeightConstraint()
        searchTableView.flashScrollIndicators()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func bindWithObserver() {
        super.bindWithObserver()
        
        observer.from(viewModel, \.searchPlaces).to { places in
            if self.isLocationSearch == true {
                self.searchTableView.reloadData()
                self.updateTableViewHeightConstraint()
            }
        }
        observer.from(viewModel, \.searchPlace).to { place in
            self.locationLabelButton.setTitle(place, for: .normal)
        }
    }
    
    private func configureSearchTextField() {
        searchTextField.cornerRadius()
        
        let searchImageView = UIImageView()
        searchImageView.contentMode = .scaleAspectFit
        searchImageView.heightAnchor.constraint(equalToConstant: 15).isActive = true
        searchImageView.widthAnchor.constraint(equalToConstant: 15).isActive = true
        searchImageView.image = Asset.search.image
        
        let searchFlexibleSpaceView1 = UIView()
        searchFlexibleSpaceView1.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        let searchFlexibleSpaceView2 = UIView()
        searchFlexibleSpaceView2.widthAnchor.constraint(equalToConstant: 10).isActive = true
        
        let searchStackView = UIStackView()
        searchStackView.addArrangedSubview(searchFlexibleSpaceView1)
        searchStackView.addArrangedSubview(searchImageView)
        searchStackView.addArrangedSubview(searchFlexibleSpaceView2)
        
        searchTextField.leftView = searchStackView
        searchTextField.leftViewMode = .always
    }
    
    private func searchBarToTopPosition() {
        isLocationSearch = false
        isCanditatesOnTopPosition = false
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 0.0
            self.popularSearchesCollectionView.alpha = 0.0
            self.locationView.alpha = 0.0
            self.searchTableView.alpha = 1.0
            self.candidatesTableView.alpha = 0.0
            self.searchTextField.alpha = 1.0

            self.searchCenterConstraint.priority = UILayoutPriority(rawValue: 1)
            self.tableViewTopPopularLabelConstraint.priority = UILayoutPriority(rawValue: 250)
            self.searchTopSuperViewConstraint.priority = UILayoutPriority(rawValue: 999)
            self.searchTopLocationConstraint.priority = UILayoutPriority(rawValue: 250)
            self.view.layoutIfNeeded()
        })
        
//        UIView.animate(withDuration: 0.5, animations: {
//            self.titleLabel.alpha = 0.0
//            self.popularSearchesCollectionView.alpha = 0.0
//            self.locationView.alpha = 0.0
//            self.searchTableView.alpha = 1.0
//            self.candidatesTableView.alpha = 0.0
//            self.searchTextField.alpha = 1.0
//
//            self.searchCenterConstraint.priority = UILayoutPriority(rawValue: 1)
//            self.tableViewTopPopularLabelConstraint.priority = UILayoutPriority(rawValue: 250)
//            self.searchTopSuperViewConstraint.priority = UILayoutPriority(rawValue: 999)
//            self.searchTopLocationConstraint.priority = UILayoutPriority(rawValue: 250)
//            self.view.layoutIfNeeded()
//        }) { isSuccess in
//            self.searchBarDefaultPosition()
//            ыудаю
//            CompositionRoot.sharedInstance.pushTabBarTo(index: 1)
//        }
    }
    
    private func searchBarDefaultPosition() {
        isLocationSearch = false
        isCanditatesOnTopPosition = false
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 1.0
            self.popularSearchesCollectionView.alpha = 1.0
            self.locationView.alpha = 1.0
            self.searchTableView.alpha = 0.0
            self.candidatesTableView.alpha = 1.0
            self.searchTextField.alpha = 1.0
            self.popularSearchesDefaultFont()
            
            self.searchCenterConstraint.priority = UILayoutPriority(rawValue: 999)
            self.tableViewTopPopularLabelConstraint.priority = UILayoutPriority(rawValue: 1000)
            self.searchTopSuperViewConstraint.priority = UILayoutPriority(rawValue: 750)
            self.searchTopLocationConstraint.priority = UILayoutPriority(rawValue: 750)
            self.popularSearchesTopSearchBarConstraint.priority = UILayoutPriority(rawValue: 1000)
            self.view.layoutIfNeeded()
        })
    }
    
    private func searchBarLocationPosition() {
        isLocationSearch = true
        isCanditatesOnTopPosition = false
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 0.0
            self.popularSearchesCollectionView.alpha = 0.0
            self.locationView.alpha = 1.0
            self.searchTableView.alpha = 1.0
            self.searchTextField.alpha = 1.0
            self.candidatesTableView.alpha = 0.0
            
            self.searchCenterConstraint.priority = UILayoutPriority(rawValue: 1)
            self.tableViewTopPopularLabelConstraint.priority = UILayoutPriority(rawValue: 250)
            self.searchTopSuperViewConstraint.priority = UILayoutPriority(rawValue: 250)
            self.searchTopLocationConstraint.priority = UILayoutPriority(rawValue: 750)
            self.view.layoutIfNeeded()
        })
    }
    
    private func canditatesToTopPosition() {
        isLocationSearch = false
        isCanditatesOnTopPosition = true
        UIView.animate(withDuration: 0.5, animations: {
            self.titleLabel.alpha = 0.0
            self.popularSearchesCollectionView.alpha = 1.0
            self.locationView.alpha = 1.0
            self.searchTableView.alpha = 0.0
            self.candidatesTableView.alpha = 1.0
            self.searchTextField.alpha = 0.0
            self.popularSearchesLargeFont()
            
            self.tableViewTopPopularLabelConstraint.priority = UILayoutPriority(rawValue: 1)
            self.popularSearchesTopSearchBarConstraint.priority = UILayoutPriority(rawValue: 1)
            self.view.layoutIfNeeded()
        })
    }
    
    private let randomColors: [UIColor] = [UIColor(red:0.08, green:0.08, blue:0.13, alpha:1.00),
                                                  UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.00),
                                                  UIColor(red:0.75, green:0.32, blue:0.95, alpha:1.00),
                                                  UIColor(red:0.41, green:0.47, blue:0.97, alpha:1.00),
                                                  UIColor(red:1.00, green:0.81, blue:0.36, alpha:1.00),
                                                  UIColor(red:0.95, green:0.60, blue:0.29, alpha:1.00),
                                                  UIColor(red:0.00, green:0.52, blue:0.96, alpha:1.00),
                                                  UIColor(red:0.00, green:0.77, blue:0.55, alpha:1.00),
                                                  UIColor(red:1.00, green:0.39, blue:0.49, alpha:1.00)]
    
    @IBAction func locationButtonPressed(_ sender: Any) {
        searchBarLocationPosition()
        searchTextField.becomeFirstResponder()
    }
    
    @objc override func dismissKeyboard() {
        searchBarDefaultPosition()
        isLocationSearch = false
        searchTextField.text = nil
        self.view.endEditing(true)
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField == searchTextField && isLocationSearch == true {
            locationSearch = textField.text
        }
    }
    
    private func popularSearchesLargeFont() {
        self.popularSearchesCollectionView.reloadData()
        self.popularSearchesCollectionView.layoutIfNeeded()
//        self.popularSearchLabel.font = UIFont(name: "Ubuntu-Bold", size: 24.0)
//        self.popularSearchLabel.textColor = .white
    }
    
    private func popularSearchesDefaultFont() {
        self.popularSearchesCollectionView.reloadData()
        self.popularSearchesCollectionView.layoutIfNeeded()
//        self.popularSearchLabel.font = UIFont(name: "Ubuntu-Medium", size: 17.0)
//        self.popularSearchLabel.textColor = .black
    }
    
    private func updateTableViewHeightConstraint() {
        let height = searchTableView.contentSize.height
        tableViewHeightConstraint.constant = height
        view.setNeedsLayout()
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchTableView {
            if isLocationSearch {
                return viewModel.searchPlaces.count
            } else {
                return similarSearchTitles.count
            }
        } else {
            if let candidates = viewModel.bestSearchesProfileModels?[popularSearchSelectedIndexPath.row].candidates {
                return candidates.count
            } else {
                return 0
            }
        }
    }
    
    func tableView(_ tableView: UITableView,
                   cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchTableViewCell") as! SearchTableViewCell
            if isLocationSearch {
                cell.configure(with: viewModel.searchPlaces[indexPath.row],
                               resultsCount: 12
                )
            } else {
                cell.configure(with: similarSearchTitles[indexPath.row],
                               resultsCount: similarSearchResultsCounts[indexPath.row])
            }
            
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "searchCard") as! SearchCardTableViewCell
            if let profile = viewModel.bestSearchesProfileModels?[popularSearchSelectedIndexPath.row].candidates[indexPath.row] {
                cell.configure(title: profile.title,
                               imageURL: profile.imageURL,
                               fullName: profile.fullName,
                               color: randomColors.randomElement() ?? .white,
                               description: profile.description)
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("indexpath: \(indexPath.row)")
        if isLocationSearch == true {
            viewModel.searchPlace = viewModel.searchPlaces[indexPath.row]
        } else {
//            Coordinator.shared.goToProfileVC(profile: viewModel.bestSearchesProfileModels?[popularSearchSelectedIndexPath.row].candidates[indexPath.row])
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let bestSearches = viewModel.bestSearchesProfileModels else { return 0 }
        return bestSearches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesCell", for: indexPath) as! PopularSearchCollectionViewCell
        if let bestSearches = viewModel.bestSearchesProfileModels?[indexPath.row] {
            cell.configure(skill: bestSearches.title,
                           selected: indexPath == popularSearchSelectedIndexPath,
                           isTop: isCanditatesOnTopPosition)
        } else {
            cell.configure(skill: "No Data",
                           selected: indexPath == popularSearchSelectedIndexPath,
                           isTop: isCanditatesOnTopPosition)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let skill = viewModel.bestSearchesProfileModels?[indexPath.row].title,
            let mediumFont = UIFont(name: "Ubuntu-Medium", size: 15.0),
            let boldFont = UIFont(name: "Ubuntu-Bold", size: 18.0) {
            // 16.0 - left + right width
            let font = popularSearchSelectedIndexPath == indexPath ? boldFont : mediumFont
            let width = skill.width(withConstrainedHeight: 26.0, font: font) + 16.0
            return CGSize(width: width, height: 28.0)
        } else {
            return CGSize(width: 179, height: 28.0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: popularSearchSelectedIndexPath),
            let skillCell = cell as? PopularSearchCollectionViewCell {
            skillCell.select(indexPath == popularSearchSelectedIndexPath, isCanditatesOnTopPosition)
        }
        
        self.popularSearchSelectedIndexPath = indexPath
        
        if let cell = collectionView.cellForItem(at: popularSearchSelectedIndexPath),
            let skillCell = cell as? PopularSearchCollectionViewCell {
            skillCell.select(indexPath == popularSearchSelectedIndexPath, isCanditatesOnTopPosition)
        }
        
        self.popularSearchesCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.popularSearchesCollectionView.collectionViewLayout.invalidateLayout()
        
        self.candidatesTableView.reloadData()
    }
    
    // MARK: HomeView Protocol
    
    func reloadBestSearchesProfileCollectionView() {
        candidatesTableView.reloadData()
    }
}

// Hack with candidatesTableView
extension HomeViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == candidatesTableView {
            print("ContentOffset: \(scrollView.contentOffset)")
            print("ContentSize: \(scrollView.contentSize)")
            print("\n")
            
            if scrollView.contentOffset.y >= 50 && isCanditatesOnTopPosition == false {
                canditatesToTopPosition()
            } else if scrollView.contentOffset.y <= 50 && isCanditatesOnTopPosition == true {
                searchBarDefaultPosition()
            }
        }
    }
    
}

extension HomeViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == searchTextField && isLocationSearch == false {
            searchBarToTopPosition()
        }
        if textField == searchTextField && isLocationSearch == true {
            locationSearch = textField.text
        }
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            searchBarDefaultPosition()
            isLocationSearch = false
            searchTextField.text = nil
            self.view.endEditing(true)
        }
        return false
    }
    
}

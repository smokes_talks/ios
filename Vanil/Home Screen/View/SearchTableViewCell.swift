import UIKit

class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var resultsCountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadius()
    }
    
    func configure(with title: String, resultsCount: Int) {
        mainLabel.text = title
//        resultsCountLabel.text = "\(resultsCount) results"
    }
    
}

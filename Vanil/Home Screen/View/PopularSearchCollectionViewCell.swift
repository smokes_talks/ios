import UIKit

class PopularSearchCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var skillLabel: UILabel!
    
    override func awakeFromNib() {
        layer.cornerRadius = 3
        layer.masksToBounds = true
    }
    
    func configure(skill: String, selected: Bool, isTop: Bool) {
        self.skillLabel.text = skill
        if selected {
            self.skillLabel.font = UIFont(name: "Ubuntu-Bold", size: 18.0)
            self.skillLabel.textColor = isTop ? .white : .black
        } else {
            self.skillLabel.font = UIFont(name: "Ubuntu-Medium", size: 15.0)
            self.skillLabel.textColor = .lightGray
        }
    }
    
    func select(_ selected: Bool, _ isTop: Bool) {
        if selected {
            self.skillLabel.font = UIFont(name: "Ubuntu-Bold", size: 18.0)
            self.skillLabel.textColor = isTop ? .white : .black
        } else {
            self.skillLabel.font = UIFont(name: "Ubuntu-Medium", size: 15.0)
            self.skillLabel.textColor = .lightGray
        }
    }
    
}

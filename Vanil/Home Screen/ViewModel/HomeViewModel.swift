import Foundation
//import GooglePlaces
import MapKit
import VanilKit

protocol HomeView {
    func reloadBestSearchesProfileCollectionView()
}

class HomeViewModel: ViewModel, MKLocalSearchCompleterDelegate {
    
    let view: HomeView
    let getBestSearchesProfiles: GetBestSearchesProfiles?
    var liqPay: LiqpayMob!
    
    @objc dynamic var searchPlace: String?
    
    @objc dynamic var searchPlaces: [String] = ["Odessa",
                                                "Kiev",
                                                "New York",
                                                "Berlin",
                                                "California",
                                                "Moscow",
                                                "Baku"]
    
    var localSearchCompleter: MKLocalSearchCompleter? {
        didSet {
            localSearchCompleter?.resultTypes = .address
            localSearchCompleter?.delegate = self
        }
    }
    
    dynamic var bestSearchesProfileModels: [PopularSearchModel]? {
        didSet {
            view.reloadBestSearchesProfileCollectionView()
        }
    }
    
    init(getBestSearchesProfiles: GetBestSearchesProfiles,
         view: HomeView) {
        self.getBestSearchesProfiles = getBestSearchesProfiles
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        localSearchCompleter = MKLocalSearchCompleter()
        fetchBestSearchesProfiles()
        
        self.liqPay = LiqpayMob(liqPayWithDelegate: self)
//        self.liqPay.
    }
    
    public func searchPlaces(text: String) {
        /// Google Places API
        /*let sessionToken = GMSAutocompleteSessionToken()
        GMSPlacesClient.shared().findAutocompletePredictions(fromQuery: text,
                                                             filter: nil,
                                                             sessionToken: sessionToken) { (response, error) in
                                                                if let _ = error {
                                                                    self.defaultSearchPlaces()
                                                                }
                                                                guard let response = response else { return }
                                                                var places: [String] = [String]()
                                                                for place in response as NSArray {
                                                                    let full = (place as! GMSAutocompletePrediction).attributedFullText.string
                                                                    places.append(full)
                                                                }
                                                                self.searchPlaces = places
        }*/
        localSearchCompleter?.queryFragment = text
    }
    
    public func fetchBestSearchesProfiles() {
        self.getBestSearchesProfiles?.get(completion: { (response, error) in
            if let _ = error {
                /// Error Handle
            }
            guard let response = response else { /* Error Handle */ return }
            
            self.bestSearchesProfileModels = response
        })
    }
    
    private func defaultSearchPlaces() {
        searchPlaces = ["Odessa", "Kiev", "New York", "Berlin", "California", "Moscow", "Baku"]
    }
    
    func completerDidUpdateResults(_ completer: MKLocalSearchCompleter) {
        var places: [String] = [String]()
        for place in completer.results {
            places.append(place.title)
        }
        self.searchPlaces = places
    }
    
}

extension HomeViewModel: LiqPayCallBack {
    
    func navigationController() -> UINavigationController! {
        return self.navigationController()
    }
            
    func onResponseSuccess(_ response: String!) {
        print(response)
    }
    
    func onResponseError(_ errorCode: Error!) {
        print(errorCode)
    }
    
    func getStatusBarColor() -> UIColor! {
        return .red
    }
    
}

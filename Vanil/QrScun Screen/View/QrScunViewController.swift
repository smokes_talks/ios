import UIKit
import CoreGraphics
import AVFoundation

class QrScunViewController: BaseViewController<QrScunViewModel>, AVCaptureMetadataOutputObjectsDelegate {
    
    var captureSession: AVCaptureSession!
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var isPresenting = false
    var isWork = UIButton()
    var descriptionLabel = UILabel()
    var failedLabel = UILabel()
    let qrCodeFrameView = UIView()
    let menuView = UIView()
    let menuHeight = UIScreen.main.bounds.height / 1.65
    let focusView = UIImageView()
    let succeededView = UIImageView()
    let shadowView = UIView()
    
    lazy var backdropView: UIView = {
        let bdView = UIView(frame: self.view.bounds)
        bdView.backgroundColor = UIColor.white.withAlphaComponent(0.5)
        return bdView
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureLabel()
        configureButton()
        configureMenuView()
        
        captureSession = AVCaptureSession()
        
        let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [.builtInDualCamera], mediaType: AVMediaType.video, position: .back)
        
        guard let captureDevice = deviceDiscoverySession.devices.first else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession.addOutput(captureMetadataOutput)
            captureSession.addInput(input)
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
        } catch {
            print(error)
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        videoPreviewLayer?.frame = menuView.layer.bounds
        videoPreviewLayer?.frame = CGRect(x: 32, y: 88, width: view.frame.width-64, height: view.frame.height/2.5)
        videoPreviewLayer?.cornerRadius = 8
        videoPreviewLayer?.layoutIfNeeded()
        menuView.layer.addSublayer(videoPreviewLayer!)
        configureFocus()
        
        captureSession.startRunning()
    }
    
    func configureFocus() {
        focusView.frame = CGRect(x: 48, y: 104, width: view.frame.width-96,
                                 height: view.frame.height/2.5 - 32)
        focusView.image = UIImage(named: "rectangle")
        menuView.addSubview(focusView)
        menuView.bringSubviewToFront(focusView)
    }
    
    func configureLabel() {
        descriptionLabel.frame = CGRect(x: 16, y: 32, width: view.frame.width-32, height: 24)
        descriptionLabel.font = UIFont(name: "Ubuntu-Medium", size: 18)
        descriptionLabel.textColor = .white
        descriptionLabel.text = "Отсканируй QR-code, чтобы сделать заказ"
        descriptionLabel.textAlignment = .center
        descriptionLabel.adjustsFontSizeToFitWidth = true
        menuView.addSubview(descriptionLabel)
    }
    
    func configureButton() {
        isWork.frame = CGRect(x: view.frame.width/2 - 90/2, y: view.frame.height/1.65 - 64, width: 90, height: 16)
        isWork.setTitle("Не работает?", for: .normal)
        isWork.setTitleColor(.darkGray, for: .normal)
        isWork.titleLabel?.font = UIFont(name: "Ubuntu-Regular", size: 14)
        
        let attributedString = NSMutableAttributedString.init(string: "Не работает?")
        attributedString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range:
            NSRange.init(location: 0, length: attributedString.length));
        
        isWork.titleLabel?.attributedText = attributedString
        menuView.addSubview(isWork)
        isWork.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
    }
    
    @objc func buttonAction(sender: UIButton!) {
        
        
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        if metadataObjects.count == 0 {
            qrCodeFrameView.frame = CGRect.zero
            return
        }
        
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObject.ObjectType.qr {
            if metadataObj.stringValue != nil {
                found(code: metadataObj.stringValue!)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                    self.configureShadow()
                    self.configureSucceeded()
                    self.captureSession.stopRunning()
                }
                
            }
        }
        
        captureSession.stopRunning()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.dismiss(animated: true)
        }
    }
    
    func found(code: String) {
        print(code)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func configureShadow() {
        shadowView.frame = videoPreviewLayer!.bounds
        shadowView.frame = CGRect(x: 32, y: 88, width: shadowView.frame.width, height: shadowView.frame.height)
        shadowView.backgroundColor = UIColor(red: 0.312, green: 0.312, blue: 0.312, alpha: 0.90)
        shadowView.cornerRadius()
        menuView.addSubview(shadowView)
    }
    
    func configureSucceeded() {
        succeededView.frame = CGRect(x: focusView.frame.width/2 - 43, y: focusView.frame.height/2 - 46, width: 86, height: 92)
        succeededView.image = UIImage(named: "succeeded")
        menuView.bringSubviewToFront(focusView)
        focusView.addSubview(succeededView)
        focusView.bringSubviewToFront(succeededView)
    }
    
    func configureFailed() {
        succeededView.frame = CGRect(x: focusView.frame.width/2 - 43, y: focusView.frame.height/2 - 46, width: 86, height: 92)
        succeededView.image = UIImage(named: "failed")
        failedLabel.frame = CGRect(x: 16, y: focusView.frame.height/2 + 48, width: focusView.frame.width - 32, height: 40)
        failedLabel.text = "Похоже, что-то пошло не так, пожалуйста, попробуй еще раз."
        failedLabel.font = UIFont(name: "Ubuntu-Regular", size: 17)
        failedLabel.adjustsFontSizeToFitWidth = true
        failedLabel.textColor = .white
        failedLabel.textAlignment = .center
        failedLabel.numberOfLines = 0
        menuView.bringSubviewToFront(focusView)
        focusView.addSubview(succeededView)
        focusView.addSubview(failedLabel)
        focusView.bringSubviewToFront(succeededView)
        focusView.bringSubviewToFront(failedLabel)
    }
    
    func configureMenuView() {
        view.backgroundColor = .clear
        view.addSubview(backdropView)
        view.addSubview(menuView)
        
        menuView.cornerRadius()
        menuView.clipsToBounds = true
        menuView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        menuView.backgroundColor = #colorLiteral(red: 0.2, green: 0.2, blue: 0.2117647059, alpha: 1)
        menuView.translatesAutoresizingMaskIntoConstraints = false
        menuView.heightAnchor.constraint(equalToConstant: menuHeight).isActive = true
        menuView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        menuView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        menuView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(QrScunViewController.handleTap(_:)))
        backdropView.addGestureRecognizer(tapGesture)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
}


extension QrScunViewController: UIViewControllerTransitioningDelegate, UIViewControllerAnimatedTransitioning {
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return self
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 1
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let containerView = transitionContext.containerView
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)
        guard let toVC = toViewController else { return }
        isPresenting = !isPresenting
        
        if isPresenting == true {
            containerView.addSubview(toVC.view)
            
            menuView.frame.origin.y += menuHeight
            backdropView.alpha = 0
            
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.menuView.frame.origin.y -= self.menuHeight
                self.backdropView.alpha = 1
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        } else {
            UIView.animate(withDuration: 0.4, delay: 0, options: [.curveEaseOut], animations: {
                self.menuView.frame.origin.y += self.menuHeight
                self.backdropView.alpha = 0
            }, completion: { (finished) in
                transitionContext.completeTransition(true)
            })
        }
    }
}

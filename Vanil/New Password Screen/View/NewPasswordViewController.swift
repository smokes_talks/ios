import UIKit

class NewPasswordViewController: BaseViewController<NewPasswordViewModel>, UITextFieldDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var sendButton: UIButton! {
        didSet {
            sendButton.cornerRadius(radius: 4)
        }
    }
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            passwordTextField.keyboardType = .default
            passwordTextField.isSecureTextEntry = true
            passwordTextField.delegate = self
            passwordTextField.tag = 200
            passwordTextField.becomeFirstResponder()
        }
    }
    
    @IBOutlet weak var passwordConfirmTextField: UITextField! {
        didSet {
            passwordConfirmTextField.keyboardType = .default
            passwordConfirmTextField.isSecureTextEntry = true
            passwordConfirmTextField.delegate = self
            passwordConfirmTextField.tag = 201
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.tintColor = .black
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField.tag {
        case 200:
            // save password
            passwordConfirmTextField.becomeFirstResponder()
            return true
        case 201:
            // check if passwords doesn't match
            // save password
            passwordConfirmTextField.resignFirstResponder()
            return true
        default:
            return false
        }
    }
    
}

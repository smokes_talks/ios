import UIKit

class NavigationBarViewPlaceCell: UICollectionViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!

    func configureWith(place: SearchPlacesModel) {
        titleLabel.text = place.addres
        typeLabel.text = place.additionalAddres
    }

}

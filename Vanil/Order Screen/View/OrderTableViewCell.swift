import UIKit

class OrderTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stepper: UIStepper!
    @IBOutlet weak var countLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        configureStepper()
    }
    
    func configureStepper() {
        stepper.isContinuous = false
        stepper.autorepeat = false
        stepper.wraps = false
        stepper.value = 1
        stepper.minimumValue = 1
        stepper.maximumValue = 20
        stepper.backgroundColor = UIColor(red: 51/255, green: 51/255, blue: 51/255, alpha: 1)
        stepper.cornerRadius(radius: 8)
        stepper.setDecrementImage(stepper.decrementImage(for: .normal), for: .normal)
        stepper.setIncrementImage(stepper.incrementImage(for: .normal), for: .normal)
        stepper.tintColor = .white
    }
    
    
    @IBAction func stepperValueChanged(_ sender: Any) {
        countLabel.text =  String(format: "%.0f", stepper.value) + "x"
        if stepper.value < 1 {
            stepper.disableBorder()
        }
    }
    
}

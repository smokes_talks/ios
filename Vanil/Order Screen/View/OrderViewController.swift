import UIKit

class OrderViewController: BaseViewController<OrderViewModel> {

    
    @IBOutlet weak var checkoutButton: UIButton!
    @IBOutlet weak var orderTableView: UITableView! {
           didSet {
               orderTableView.delegate = self
               orderTableView.dataSource = self
           }
       }
    
    let compositionRoot = CompositionRoot.sharedInstance
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Ваш заказ"
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        checkoutButton.cornerRadius(radius: 8)
    }
    
    // MARK: - Navigation

    @IBAction func checkoutActionButton(_ sender: Any) {
        let vc = compositionRoot.resolveQrScunViewController()
        vc.modalPresentationStyle = .custom
        present(vc, animated: true, completion: nil)
    }
}

extension OrderViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderTableViewCell", for: indexPath) as! OrderTableViewCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 128
    }
    
}

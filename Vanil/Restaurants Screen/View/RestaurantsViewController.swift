import UIKit

class RestaurantsViewController: BaseViewController<RestaurantsViewModel> {

    
    
    @IBOutlet weak var restaurantsTableView: UITableView! {
        didSet {
            restaurantsTableView.delegate = self
            restaurantsTableView.dataSource = self
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.title = "Рестораны"

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = false
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.white]
    }
    
    @IBAction func locationButton(_ sender: Any) {
//        Coordinator.shared.goToMap()
    }
    
}

extension RestaurantsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 300
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "restaurantTableViewCell") as! RestaurantTableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let vc = CompositionRoot.sharedInstance.resolveContentViewController()//change this to your class name
//        self.present(vc, animated: true, completion: nil)

    }
    
}

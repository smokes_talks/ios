import UIKit

class IntroductionViewController: BaseViewController<IntroductionViewModel> {
    
    @IBOutlet weak var startButton: UIButton! {
        didSet {
            startButton.cornerRadius()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func startButtonPressed(_ sender: Any) {
        goToSignInScreen()
    }
    
    func goToSignInScreen() {
        Coordinator.shared.goToSignInVC()
    }
}

import UIKit

class NotificationsViewController: BaseViewController<NotificationsViewModel> {
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationItem.title = "Notifications"
        
        let settingsButton = UIButton(type: .custom)
        settingsButton.frame = CGRect(x: 0.0, y: 0.0, width: 18, height: 21)
        settingsButton.setImage(Asset.filter.image, for: .normal)
        settingsButton.addTarget((self), action: #selector(filterButtonPressed), for: UIControl.Event.touchUpInside)
        let settingsBarButton = UIBarButtonItem(customView: settingsButton)
        
        let settingsBarButtonWidth = settingsBarButton.customView?.widthAnchor.constraint(equalToConstant: 30)
        settingsBarButtonWidth?.isActive = true
        
        let settingsBarButtonHeight = settingsBarButton.customView?.heightAnchor.constraint(equalToConstant: 30)
        settingsBarButtonHeight?.isActive = true
        
        navigationItem.rightBarButtonItem = settingsBarButton
    }
    
    @objc func filterButtonPressed() {
         goToNotificationsSettings()
    }
    
    // MARK: - Private methods
    private func goToNotificationsSettings() {
        Coordinator.shared.goToNotificationsSettingsVC()
    }
}


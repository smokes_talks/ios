import UIKit

struct HeaderCell {
    let header: String
    let setting: [SettingCell]
}

struct SettingCell {
    let image: UIImage
    let title: String
    let description: String
}

class SettingsViewController: BaseViewController<SettingsViewModel> {
    
    @IBOutlet weak var settingsTableView: UITableView! {
        didSet {
            settingsTableView.isScrollEnabled = false
            settingsTableView.delegate = self
            settingsTableView.dataSource = self
        }
    }
    @IBOutlet weak var settingsTableViewHeightConstraint: NSLayoutConstraint!
    
    private let settings: [HeaderCell] = [HeaderCell(header: "Account settings",
                                                     setting: [SettingCell(image: Asset.settingsProfile.image,
                                                                           title: "Profile Information",
                                                                           description: "Name, Hot buttons, Country")]),
                                          HeaderCell(header: "Notifications settings",
                                                     setting: [SettingCell(image: Asset.settingsNotifications.image,
                                                                           title: "Push Notifications",
                                                                           description: "when somebody interested in you")]),
                                          HeaderCell(header: "General",
                                                     setting: [SettingCell(image: Asset.settingsRateOurApp.image,
                                                                           title: "Profile Information",
                                                                           description: "Name, Hot buttons, Country"),
                                                               SettingCell(image: Asset.settingsFeedback.image,
                                                                           title: "Profile Information",
                                                                           description: "Name, Hot buttons, Country"),
                                                               SettingCell(image: Asset.settingsPrivacyPolicy.image,
                                                                           title: "Profile Information",
                                                                           description: "Name, Hot buttons, Country")])
    ]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.largeTitleDisplayMode = .never
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        settingsTableViewHeightConstraint.constant = settingsTableView.contentSize.height
    }
    
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let header = settings[section]
        return header.setting.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingCellIdentifier") as! SettingTableViewCell
        let setting = settings[indexPath.section].setting[indexPath.row]
        cell.configure(with: setting.image, title: setting.title, description: setting.description)
        return cell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 40.0))
         
        let label = UILabel(frame: CGRect(x: 16, y: 0, width: headerView.frame.width, height: 40.0))
        label.font = UIFont(name: "Ubuntu-Regular", size: 15.0)
        label.text = settings[section].header
        
        headerView.addSubview(label)

        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
//    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        let header = settings[section]
//        return header.header
//    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settings.count
    }
    
}

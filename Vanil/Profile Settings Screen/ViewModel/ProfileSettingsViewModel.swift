import UIKit

class ProfileSettingsViewModel: ViewModel {
    
    let headersArray = ["User.my_profile", "User.bank_details"]
    
    let keyboardsTypes: [[UIKeyboardType]] =
        [
            [UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.emailAddress,
             UIKeyboardType.phonePad,
             UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.numberPad],
            
            [UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.default,
             UIKeyboardType.default]
    ]
    
    let settingsTextsArray =
        [
            ["First Name",
             "Last Name",
             "Email",
             "Phone",
             "Date of Birth",
             "Country",
             "Prospects/State/Province/Region",
             "Address",
             "City",
             "Postal Code"],
            
            ["Bank Name",
             "Bank Address",
             "IBAN",
             "BIC"]
        ]
    
    let placeholdersArray =
        [
            ["enter name",
             "enter lastname",
             "enter email",
             "enter phonenumber",
             "enter date of birth",
             "enter country",
             "enter prospects/state/province/region",
             "enter adress",
             "enter city",
             "enter postal code"],
            
            ["enter bank name",
             "enter bank adress",
             "enter IBAN",
             "enter BIC"]
        ]
}

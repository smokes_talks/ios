import UIKit

class ProfileSettingsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var profileSettingLabel: UILabel!
    @IBOutlet weak var profileSettingTextField: UITextField!
    
    func configure(with settingText: String, placeholderText: String, keyboardType: UIKeyboardType) {
        profileSettingLabel.text = settingText
        profileSettingTextField.placeholder = placeholderText
        profileSettingTextField.keyboardType = keyboardType
    }
    
}

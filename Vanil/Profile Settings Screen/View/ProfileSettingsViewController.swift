import UIKit

class ProfileSettingsViewController: BaseViewController<ProfileSettingsViewModel>, UITableViewDelegate, UITableViewDataSource {
    
    private let kDateBirtdayIndexPath = IndexPath(row: 4, section: 0)
    
    // MARK: - Outlets
    
    @IBOutlet weak var profileSettingsTableView: UITableView! {
        didSet {
            profileSettingsTableView.delegate = self
            profileSettingsTableView.dataSource = self
        }
    }
    
    @objc func tapDone() {
        print("textfield")
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.4561589956, green: 0.6784378886, blue: 0.4503215551, alpha: 1)]
        
        navigationItem.title = "Settings"
        
        self.hidingKeyboardSettings()
    }
    
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.headersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 10
        case 1:
            return 4
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "profileSettingsCell") as! ProfileSettingsTableViewCell
        
        cell.configure(with: viewModel.settingsTextsArray[indexPath.section][indexPath.row],
                       placeholderText: viewModel.placeholdersArray[indexPath.section][indexPath.row],
                       keyboardType: viewModel.keyboardsTypes[indexPath.section][indexPath.row])
        
        if indexPath == kDateBirtdayIndexPath {
            cell.profileSettingTextField.setInputViewDatePicker(target: self, selector: #selector(tapDone))
        } else {
            cell.profileSettingTextField.clearInputViewDatePicker()
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 104
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headersArray[section]
    }
    
}


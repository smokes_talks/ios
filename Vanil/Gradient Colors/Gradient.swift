import UIKit

class Gradient {
    static private var gradient1: [UIColor] = [ColorName.gradient11,
                                               ColorName.gradient12,
                                               ColorName.gradient13,
                                               ColorName.gradient14,
                                               ColorName.gradient15,
                                               ColorName.gradient16]
    static private var gradient2: [UIColor] = [ColorName.gradient21,
                                               ColorName.gradient22,
                                               ColorName.gradient23,
                                               ColorName.gradient24,
                                               ColorName.gradient25,
                                               ColorName.gradient26]
    static private var gradient3: [UIColor] = [ColorName.gradient31,
                                               ColorName.gradient32,
                                               ColorName.gradient33,
                                               ColorName.gradient34,
                                               ColorName.gradient35,
                                               ColorName.gradient36]
    static private var gradient4: [UIColor] = [ColorName.gradient41,
                                               ColorName.gradient42,
                                               ColorName.gradient43,
                                               ColorName.gradient44,
                                               ColorName.gradient45,
                                               ColorName.gradient46]
    static private var gradient5: [UIColor] = [ColorName.gradient51,
                                               ColorName.gradient52,
                                               ColorName.gradient53,
                                               ColorName.gradient54,
                                               ColorName.gradient55,
                                               ColorName.gradient56]
    static private var colors: [[UIColor]] = [gradient1, gradient2, gradient3, gradient4, gradient5]
    
    static func random() -> [UIColor] {
        return colors.randomElement() ?? gradient1
    }
    
    static func randomGradientSet() -> [[CGColor]] {
        var gradientSet: [[CGColor]] = [[CGColor]]()
        
        let colors = random()
        var index = 0
        for _ in 0..<colors.count/2 {
            gradientSet.append([colors[index].cgColor, colors[index+1].cgColor])
            index += 2
        }
        return gradientSet
    }
}

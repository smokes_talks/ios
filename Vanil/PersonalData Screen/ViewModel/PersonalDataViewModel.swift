import Foundation
import VanilKit

public struct PersonData {
    var title: String
    var isHidden: Bool
    var parametrs: [String]!
}

class PersonalDataViewModel: ViewModel {
    
    private let authenticationAPIManager: AuthenticationAPIManager
    
    let personalData: [PersonData] = [PersonData(title: "Введите имя", isHidden: true, parametrs: nil),
                                      PersonData(title: "Введите фамилию", isHidden: true, parametrs: nil),
                                      PersonData(title: "Введите e-mail", isHidden: true, parametrs: nil),
                                      PersonData(title: "Дата рождения", isHidden: true, parametrs: nil),
                                      PersonData(title: "Выберите пол", isHidden: false, parametrs: ["", "Мужской", "Женский", "Другой"]),
                                      PersonData(title: "Город", isHidden: false, parametrs: ["", "Одесса"])]
    
    init(authenticationAPIManager: AuthenticationAPIManager) {
        self.authenticationAPIManager = authenticationAPIManager
    }
    
    func authenticateUserWith(username: String,
                              andPassword password: String,
                              errorHandler: @escaping (String, String) -> Void,
                              completionHandler: @escaping (Bool) -> Void) {
        
        self.authenticationAPIManager.authenticateUser(username: username,
                                                           andPassword: password,
                                                           completionHandler:
            { (token) in
                if let tokenFin = token {
                    UserDefaults.standard.set(tokenFin, forKey: "accessToken")
                }
                completionHandler(true)
        }) { (errorStatus) in
            let message = AuthenticationAPIErrorStatus.messageForUserByStatusCode(statusCode: errorStatus)
            errorHandler(message!.title, message!.message)
            completionHandler(false)
        }
    }
}

import UIKit

class PersonalDataViewController: BaseViewController<PersonalDataViewModel> {
    
    @IBOutlet var doneBar: UIToolbar!
    @IBOutlet var genderPickerView: UIPickerView!
    @IBOutlet var cityPickerView: UIPickerView!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var personalDataTableView: UITableView! {
        didSet {
            personalDataTableView.delegate = self
            personalDataTableView.dataSource = self
        }
    }
    
    var gender = [String]()
    var city = [String]()
    
    var indexPath = IndexPath()
    var indexPath1 = IndexPath()
    var indexPath2 = IndexPath()
    
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    // MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        navigationItem.backBarButtonItem?.tintColor = .clear
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        genderPickerView.selectRow(0, inComponent: 0, animated: true)
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        
        cityPickerView.selectRow(0, inComponent: 0, animated: true)
        cityPickerView.delegate = self
        cityPickerView.dataSource = self
        
        configureBackground()
        configureButton()
    }
    
    func configureBackground() {
        let layer0 = CAGradientLayer()
        layer0.colors = [
            UIColor(red: 0.15, green: 0.168, blue: 0.183, alpha: 1).cgColor,
            UIColor(red: 0.085, green: 0.099, blue: 0.115, alpha: 1).cgColor
        ]
        layer0.locations = [0.03, 0.77]
        layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 1, c: -1, d: 0.21, tx: 0.5, ty: -0.11))
        layer0.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.5*view.bounds.size.height)
        layer0.position = view.center
        self.view.layer.insertSublayer(layer0, at: 0)
    }
    
    func configureButton() {
        let layer1 = CAGradientLayer()
        layer1.colors = [
            UIColor(red: 0.137, green: 0.145, blue: 0.149, alpha: 1).cgColor,
            UIColor(red: 0.255, green: 0.263, blue: 0.271, alpha: 1).cgColor
        ]
        layer1.locations = [0, 1]
        layer1.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer1.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer1.bounds = view.bounds.insetBy(dx: -0.5*saveButton.bounds.size.width, dy: -0.1*saveButton.bounds.size.height)
        layer1.position = view.center
        self.saveButton.layer.insertSublayer(layer1, at: 0)
        self.saveButton.clipsToBounds = true
        self.saveButton.cornerRadius(radius: 8)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        appDelegate.registerForPushNotifications()
        Coordinator.shared.goToMenu()
    }
    
    @IBAction func doneButton(_ sender: Any) {
        let cell = personalDataTableView.cellForRow(at: indexPath) as! PersonalDataTableViewCell
        cell.dataTextField.text = pickerData
        cell.dataTextField.resignFirstResponder()
        cell.dataTextField.endEditing(true)
        doneBar.isHidden = true
        pickerData = nil
    }
    
}

extension PersonalDataViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.personalData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "personalDataTableViewCell") as! PersonalDataTableViewCell
        cell.configure(title: viewModel.personalData[indexPath.row].title, isHidden: viewModel.personalData[indexPath.row].isHidden)
        
        if (indexPath.row == 4) {
            self.gender = viewModel.personalData[indexPath.row].parametrs
            cell.dataTextField.inputView = genderPickerView
            cell.dataTextField.inputAccessoryView = doneBar
            indexPath1 = indexPath
            if let pickerData = pickerData {
                cell.dataTextField.text = pickerData
            }
        }
        if (indexPath.row == 5) {
            doneBar.isHidden = false
            self.city = viewModel.personalData[indexPath.row].parametrs
            cell.dataTextField.inputView = cityPickerView
            cell.dataTextField.inputAccessoryView = doneBar
            indexPath2 = indexPath
            if let pickerData = pickerData {
                cell.dataTextField.text = pickerData
            }
        }
        return cell
    }
    
}

extension PersonalDataViewController: UIPickerViewDataSource, UIPickerViewDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPickerView {
            return gender.count
        } else {
            return city.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        pickerView.endEditing(true)
        doneBar.isHidden = false
        if pickerView == genderPickerView {
            self.indexPath = indexPath1
            return gender[row]
        } else {
            self.indexPath = indexPath2
            return city[row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPickerView {
            pickerData = gender[row]
        } else {
            pickerData = city[row]
        }
        
    }

}

import UIKit

var pickerData: String!

class PersonalDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var dataTextField: UITextField!
    @IBOutlet weak var moreView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dataTextField.delegate = self
    }
    
    func configure(title: String, isHidden: Bool) {
        self.dataTextField.attributedPlaceholder = NSAttributedString(string: title, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.51, green: 0.51, blue: 0.51, alpha: 1)])
        self.moreView.isHidden = isHidden
    }
    
}

extension PersonalDataTableViewCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


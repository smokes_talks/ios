import UIKit

class SignUpViewController: BaseViewController<SignUpViewModel> {

    
    // MARK: - IBOutlets
    
    @IBOutlet weak var createAccountButton: UIButton! {
        didSet {
            createAccountButton.cornerRadius(radius: 4)
        }
    }
    
    @IBOutlet weak var passwordTextField: UITextField! {
        didSet {
            let righButton = UIButton(type: .infoDark)
            righButton.setImage(Asset.eye.image, for: .normal)
            righButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: -16, bottom: 0, right: 0)
            righButton.addTarget(self,
                                     action: #selector(righButtonPressed),
                                     for: .touchUpInside)
            passwordTextField.rightView = righButton
            passwordTextField.rightViewMode = .always
        }
    }
    
    private var isRightBarButtonState: Bool = false
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @objc private func righButtonPressed() {
        if isRightBarButtonState {
            (passwordTextField.rightView as! UIButton).setImage(Asset.eye.image, for: .normal)
            passwordTextField.isSecureTextEntry = true
            isRightBarButtonState = false
        } else {
            (passwordTextField.rightView as! UIButton).setImage(Asset.eyeOff.image, for: .normal)
            passwordTextField.isSecureTextEntry = false
            isRightBarButtonState = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.tintColor = .black
    }

}

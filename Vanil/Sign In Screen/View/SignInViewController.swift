import UIKit
import PhoneNumberKit

class SignInViewController: BaseViewController<SignInViewModel> {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: " ", style: .plain, target: nil, action: nil)
        navigationController?.navigationBar.tintColor = .black
        tabBarController?.tabBar.isHidden = true
        configureBackground()
    }
    
    func configureBackground() {
        let layer0 = CAGradientLayer()
        layer0.colors = [
          UIColor(red: 0.15, green: 0.168, blue: 0.183, alpha: 1).cgColor,
          UIColor(red: 0.085, green: 0.099, blue: 0.115, alpha: 1).cgColor
        ]
        layer0.locations = [0.03, 0.77]
        layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 1, c: -1, d: 0.21, tx: 0.5, ty: -0.11))
        layer0.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.5*view.bounds.size.height)
        layer0.position = view.center
        self.view.layer.insertSublayer(layer0, at: 0)
    }
    
    // MARK: - IBActions
    
    @IBAction func PhoneNumberAction(_ sender: Any) {
        Coordinator.shared.goToPhoneNumber()
    }
    
   
    
    // MARK: - Private methods
    private func goToSignUp() {
        Coordinator.shared.goToSignUpVC()
    }
    
    private func goToForgotPassword() {
        Coordinator.shared.goToForgotPasswordVC()
    }
    
//    private func goToProjectsVC() {
//        Coordinator.shared.goToProjectsVC()
//    }
    
    @objc private func endEditing() {
        view.endEditing(true)
    }
    
    // MARK: - Alert Controllers
    private func showWrongUserAlertWithTitle(title: String,
                                             andMessage message: String) {
        let ac = UIAlertController(title: title,
                                   message: message,
                                   preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        ac.addAction(ok)
        self.present(ac, animated: true)
    }
    
    private func showNoNamePasswordAlert() {
        let ac = UIAlertController(title: "Enter login/email and password",
                                   message: "You need to enter login/email and password",
                                   preferredStyle: .alert)
        let ok = UIAlertAction(title: "OK", style: .default)
        ac.addAction(ok)
        self.present(ac, animated: true)
    }
}


import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var photoImageView: UIImageView! {
        didSet {
            photoImageView.cornerRadius()
        }
    }
    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel.textColor = .white
        }
    }
    
    
    override func awakeFromNib() {
        self.cornerRadius()
        self.contentView.cornerRadius()
    }

}

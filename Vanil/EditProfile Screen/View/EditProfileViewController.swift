import UIKit

class EditProfileViewController: BaseViewController<EditProfileViewModel> {
    
    @IBOutlet var genderPickerView: UIPickerView!
    @IBOutlet var cityPickerView: UIPickerView!
    @IBOutlet var doneBar: UIToolbar!
    
    @IBOutlet weak var saveButton: UIButton! {
        didSet {
            saveButton.cornerRadius()
        }
    }
    @IBOutlet weak var editProfileTableView: UITableView! {
        didSet {
            editProfileTableView.delegate = self
            editProfileTableView.dataSource = self
        }
    }
    var gender = [String]()
    var city = [String]()
    
    var indexPath = IndexPath()
    var indexPath1 = IndexPath()
    var indexPath2 = IndexPath()
    
    let titleLabel = UILabel()
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        tabBarController?.tabBar.isHidden = false
        titleLabel.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureTitle()
        tabBarController?.tabBar.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        genderPickerView.selectRow(0, inComponent: 0, animated: true)
        genderPickerView.delegate = self
        genderPickerView.dataSource = self
        
        cityPickerView.selectRow(0, inComponent: 0, animated: true)
        cityPickerView.delegate = self
        cityPickerView.dataSource = self
    }
    
    @IBAction func doneButton(_ sender: Any) {
        let cell = editProfileTableView.cellForRow(at: indexPath) as! EditProfileTableViewCell
        cell.titleTextField.text = pickerProfileData
        cell.titleTextField.resignFirstResponder()
        cell.titleTextField.endEditing(true)
        doneBar.isHidden = true
        pickerProfileData = nil
    }
    
    @objc private func handleKeyboard(notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        
        guard let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        let keyboardViewEndFrame = view.convert(keyboardScreenEndFrame, from: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            editProfileTableView.contentInset = .zero
        } else {
            editProfileTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height-100, right: 0)
        }
        
        editProfileTableView.scrollIndicatorInsets = editProfileTableView.contentInset
    }
    
    func configureTitle() {
        titleLabel.text = "Редактирование профиля"
        titleLabel.font = .boldSystemFont(ofSize: 18)
        titleLabel.textColor = .white
        titleLabel.frame = CGRect(x: (navigationController?.navigationBar.frame.width)!/2 - 233/2 , y: (navigationController?.navigationBar.frame.height)!-44, width: 233, height: 41)
        navigationController?.navigationBar.addSubview(titleLabel)
    }
    
    @IBAction func saveButtonPressed(_ sender: Any) {
    }
    
    
}

extension EditProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "editProfileTableViewCell", for: indexPath) as! EditProfileTableViewCell
        cell.selectionStyle = .none
        cell.configure(title: viewModel.profileData[indexPath.row].title, placeholderTitle: viewModel.profileData[indexPath.row].placeholderTitle, state: viewModel.profileData[indexPath.row].isHidden)
        
        if (indexPath.row == 4) {
            self.gender = viewModel.profileData[indexPath.row].parametrs
            cell.titleTextField.inputView = genderPickerView
            cell.titleTextField.inputAccessoryView = doneBar
            indexPath1 = indexPath
            if let pickerProfileData = pickerProfileData {
                cell.titleTextField.text = pickerProfileData
            }
        }
        if (indexPath.row == 5) {
            doneBar.isHidden = false
            self.city = viewModel.profileData[indexPath.row].parametrs
            cell.titleTextField.inputView = cityPickerView
            cell.titleTextField.inputAccessoryView = doneBar
            indexPath2 = indexPath
            if let pickerProfileData = pickerProfileData {
                cell.titleTextField.text = pickerProfileData
            }
        }
        return cell
    }

}

extension EditProfileViewController: UIPickerViewDataSource, UIPickerViewDelegate {

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == genderPickerView {
            return gender.count
        } else {
            return city.count
        }
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        pickerView.endEditing(true)
        doneBar.isHidden = false
        if pickerView == genderPickerView {
            self.indexPath = indexPath1
            return gender[row]
        } else {
            self.indexPath = indexPath2
            return city[row]
        }
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == genderPickerView {
            pickerProfileData = gender[row]
        } else {
            pickerProfileData = city[row]
        }
        
    }

}

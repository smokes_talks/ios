import UIKit

var pickerProfileData: String!

class EditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var moreView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        titleTextField.delegate = self
    }

    func configure(title: String, placeholderTitle: String, state: Bool) {
        self.titleTextField.text = title
        self.titleTextField.attributedPlaceholder = NSAttributedString(string: placeholderTitle, attributes: [NSAttributedString.Key.foregroundColor: UIColor(red: 0.51, green: 0.51, blue: 0.51, alpha: 1)])
        self.moreView.isHidden = state
    }
    
}

extension EditProfileTableViewCell: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

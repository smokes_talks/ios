import Foundation
import VanilKit

struct Profile {
    var title: String
    var placeholderTitle: String
    var isHidden: Bool
    var parametrs: [String]!
}

class EditProfileViewModel: ViewModel {
    
    let profileData: [Profile] = [Profile(title: "Эльсик", placeholderTitle: "Имя", isHidden: true, parametrs: nil),
                                   Profile(title: "Штеер", placeholderTitle: "Фамилия", isHidden: true, parametrs: nil),
                                   Profile(title: "elsik@gmail.com", placeholderTitle: "e-mail", isHidden: true, parametrs: nil),
                                   Profile(title: "16 октября", placeholderTitle: "Дата рождения", isHidden: true, parametrs: nil),
                                   Profile(title: "Женщина", placeholderTitle: "Пол", isHidden: false, parametrs: ["", "Мужской", "Женский", "Другой"]),
                                   Profile(title: "Одесса", placeholderTitle: "Город", isHidden: false, parametrs: ["", "Одесса"])]
    
    
    
}


import UIKit

class GreetingViewController: BaseViewController <GreetingViewModel> {
    
    @IBOutlet weak var signInButton: UIButton! {
        didSet {
            signInButton.cornerRadius(radius: 4)
        }
    }
    @IBOutlet weak var skipButton: UIButton! {
        didSet {
            skipButton.cornerRadius(radius: 4)
        }
    }
    @IBAction func signInPressed(_ sender: UIButton) {
        goToSignInVC()
    }
    
    @IBAction func skipPressed(_ sender: UIButton) {
//        goToProjectsVC()
    }
    
    // MARK: - Private methods
    private func goToSignInVC() {
        Coordinator.shared.goToSignInVC()
    }
    
//    private func goToProjectsVC() {
//        Coordinator.shared.goToProjectsVC()
//    }
    
}

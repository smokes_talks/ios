import Foundation
import VanilKit

protocol SplashScreenView: BaseHandshakesUI {
    func displayPreview(name: String, message: String, url: String)
    func goToSignInScreen()
}

class SplashScreenViewModel: ViewModel {
    
    private weak var view: SplashScreenView?
    private let getAppVersionAndCheckIt: GetAppVersionAndCheckIt
    
    init(view: SplashScreenView,
         getAppVersionAndCheckIt: GetAppVersionAndCheckIt) {
        self.view = view
        self.getAppVersionAndCheckIt = getAppVersionAndCheckIt
    }
    
    override func viewDidLoad() {
        checkIsAuthorized()
    }
    
    func checkAppVersion() {
        getAppVersionAndCheckIt.check { (name, message, url) in
            guard let name = name, let message = message, let url = url else { self.checkIsAuthorized(); return }
            self.view?.displayPreview(name: name, message: message, url: url)
        }
    }
    
    func checkIsAuthorized() {
        if let _ = UserDefaults.standard.value(forKey: kHandshakesToken) as? String,
            let tokenExpires = UserDefaults.standard.value(forKey: kTokenExpires) as? String {
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            if let expiresDate = dateFormatter.date(from: tokenExpires) {
                if Date().timeIntervalSince(expiresDate) < 0 {
//                    self.view?.goToTestListScreen()
                } else {
                    self.view?.goToSignInScreen()
                }
            } else {
                self.view?.goToSignInScreen()
            }
        } else {
            self.view?.goToSignInScreen()
        }
    }
    
}

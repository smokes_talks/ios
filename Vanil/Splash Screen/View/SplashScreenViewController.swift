import UIKit
import VanilKit

class SplashScreenViewController: BaseViewController<SplashScreenViewModel> {

    override func loadView() {
        super.loadView()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.checkAppVersion()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    func goToSignInScreen() {
        Coordinator.shared.goToSignInVC()
    }
    
}

extension SplashScreenViewController: SplashScreenView {
    
    func displayPreview(name: String, message: String, url: String) {
        let alert = createAlertToOpenAppStore(name: name, message: message, handler: {
            if let url = URL(string: url) {
                UIApplication.shared.open(url,
                                          options: [:], completionHandler: nil)
            }
        })
        present(alert, animated: true, completion: nil)
    }
    
    private func createAlertToOpenAppStore(name: String, message: String, handler: @escaping () -> Void) -> UIAlertController {
        let alertController = UIAlertController(title: name,
                                                message: message,
                                                preferredStyle: .alert)
        let okAction = UIAlertAction(title: "L10n.Test.Screen.Notification.Leavingapp.ok",
                                     style: .default) { _ in handler() }
        alertController.addAction(okAction)
        return alertController
    }
    
}

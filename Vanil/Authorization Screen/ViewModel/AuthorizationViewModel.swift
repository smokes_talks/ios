import Foundation
import VanilKit

class AuthorizationViewModel: ViewModel {
    
    private let authenticationAPIManager: AuthenticationAPIManager
    
    init(authenticationAPIManager: AuthenticationAPIManager) {
        self.authenticationAPIManager = authenticationAPIManager
    }
    
    func authenticateUserWith(username: String,
                              andPassword password: String,
                              errorHandler: @escaping (String, String) -> Void,
                              completionHandler: @escaping (Bool) -> Void) {
        
        self.authenticationAPIManager.authenticateUser(username: username,
                                                           andPassword: password,
                                                           completionHandler:
            { (token) in
                if let tokenFin = token {
                    UserDefaults.standard.set(tokenFin, forKey: "accessToken")
                }
                completionHandler(true)
        }) { (errorStatus) in
            let message = AuthenticationAPIErrorStatus.messageForUserByStatusCode(statusCode: errorStatus)
            errorHandler(message!.title, message!.message)
            completionHandler(false)
        }
    }
}

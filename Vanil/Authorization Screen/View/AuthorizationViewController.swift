import UIKit
import Firebase
import FirebaseAuth


class AuthorizationViewController: BaseViewController<AuthorizationViewModel> {
    
    @IBOutlet var keyboardView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var incorrectCodeImage: UIImageView!
    
    @IBOutlet weak var firstTextField: UITextField!
    @IBOutlet weak var secondTextField: UITextField!
    @IBOutlet weak var thirdTextField: UITextField!
    @IBOutlet weak var fourthTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        firstTextField.delegate = self
        secondTextField.delegate = self
        thirdTextField.delegate = self
        fourthTextField.delegate = self
        firstTextField.becomeFirstResponder()
        
        firstTextField.tag = 1
        secondTextField.tag = 2
        thirdTextField.tag = 3
        fourthTextField.tag = 4
        
        incorrectCodeImage.isHidden = true
        configureBackground()
        configureButton()
        
        navigationController?.navigationBar.tintColor = UIColor(red: 0.949, green: 0.788, blue: 0.298, alpha: 1)
    }
    
    func configureBackground() {
        let layer0 = CAGradientLayer()
        layer0.colors = [
            UIColor(red: 0.15, green: 0.168, blue: 0.183, alpha: 1).cgColor,
            UIColor(red: 0.085, green: 0.099, blue: 0.115, alpha: 1).cgColor
        ]
        layer0.locations = [0.03, 0.77]
        layer0.startPoint = CGPoint(x: 0.25, y: 0.5)
        layer0.endPoint = CGPoint(x: 0.75, y: 0.5)
        layer0.transform = CATransform3DMakeAffineTransform(CGAffineTransform(a: 1, b: 1, c: -1, d: 0.21, tx: 0.5, ty: -0.11))
        layer0.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.5*view.bounds.size.height)
        layer0.position = view.center
        self.view.layer.insertSublayer(layer0, at: 0)
    }
    
    func configureButton() {
        let layer1 = CAGradientLayer()
        layer1.colors = [
            UIColor(red: 0.137, green: 0.145, blue: 0.149, alpha: 1).cgColor,
            UIColor(red: 0.255, green: 0.263, blue: 0.271, alpha: 1).cgColor
        ]
        layer1.locations = [0, 1]
        layer1.startPoint = CGPoint(x: 0.75, y: 0.5)
        layer1.endPoint = CGPoint(x: 0.25, y: 0.5)
        layer1.bounds = view.bounds.insetBy(dx: -0.5*view.bounds.size.width, dy: -0.1*nextButton.bounds.size.height)
        layer1.position = view.center
        self.nextButton.layer.insertSublayer(layer1, at: 0)
        self.nextButton.bringSubviewToFront(self.nextButton.imageView!)
        self.nextButton.clipsToBounds = true
    }
    
    @IBAction func nextButtonAction(_ sender: Any) {
        let countCode = firstTextField.text!.count + secondTextField.text!.count + thirdTextField.text!.count + fourthTextField.text!.count
        if (countCode < 4){
            self.incorrectCodeImage.isHidden = false
        } else {
            let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
            let verificationCode = firstTextField.text! + secondTextField.text! + thirdTextField.text! + fourthTextField.text!
            let credential = PhoneAuthProvider.provider().credential(
                withVerificationID: verificationID!,
                verificationCode: verificationCode)
            
            Auth.auth().signIn(with: credential) { (authResult, error) in
                if error != nil {
                    // ...
                    return
                }
                // User is signed in
                let currentUser = Auth.auth().currentUser
                currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                    if error != nil {
                        // Handle error
                        print("IDTokenForcingRefresh --- \(String(describing: idToken))")
                        return
                    }
                }
            }
            Coordinator.shared.goToPersonalData()
        }
    }
    
}

extension AuthorizationViewController: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        self.incorrectCodeImage.isHidden = true
        
        let maxLength = 1
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        
        if ((textField.text!.count) < 1  && string.count > 0){
            let nextTag = textField.tag + 1
            
            // get next responder
            var nextResponder = textField.superview?.viewWithTag(nextTag)
            
            if (nextResponder == nil){
                nextResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = string;
            if textField.tag < 4 {
                nextResponder?.becomeFirstResponder()
            }
            return false
        }
        else if ((textField.text!.count) >= 1  && string.count == 0){
            // on deleteing value from Textfield
            let previousTag = textField.tag - 1
            
            // get previous responder
            var previousResponder = textField.superview?.viewWithTag(previousTag)
            
            if (previousResponder == nil){
                previousResponder = textField.superview?.viewWithTag(1)
            }
            textField.text = ""
            previousResponder?.becomeFirstResponder()
            return false
        }
        
        return newString.length <= maxLength
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.text!.count == 1 {
            textField.endEditing(true)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.keyboardType = .numberPad
        textField.isHighlighted = true
        textField.inputAccessoryView = keyboardView
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

import UIKit

class SupportTableViewCell: UITableViewCell {
    
    @IBOutlet weak var supportQuestionLabel: UILabel!
    @IBOutlet weak var arrowImageView: UIImageView!
    var isCellSelected = false
    
    func configureCellWith(question: String, isSelected: Bool, imageName: String) {
        supportQuestionLabel.text = question
        isCellSelected = isSelected
        arrowImageView.image = UIImage(named: imageName)
    }
    
}

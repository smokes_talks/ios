import UIKit

class SupportViewController: BaseViewController<SupportViewModel>, UITableViewDataSource, UITableViewDelegate {
    
    
    // MARK: - Outlets
    
    @IBOutlet weak var supportTableView: UITableView! {
        didSet {
            supportTableView.delegate = self
            supportTableView.dataSource = self
        }
    }
    
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    // MARK: - UITableViewDataSource
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.headersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        case 1:
            return 5
        case 2:
            return 3
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "supportCell") as! SupportTableViewCell
        
        let question = viewModel.supportQuestions[indexPath.section][indexPath.row]
        let questionWithAnswer = viewModel.supportQuestions[indexPath.section][indexPath.row] + "\n\n" + "\(self.viewModel.supportTexts[0])"
        
        if !self.viewModel.isSelected[indexPath.section][indexPath.row] {
            cell.configureCellWith(question: question, isSelected: true, imageName: "cellUnselected")
        } else {
            cell.configureCellWith(question: questionWithAnswer, isSelected: false, imageName: "cellSelected")
        }
        cell.layoutSubviews()
        
        return cell
    }
    
    
    // MARK: - UITableViewDelegate
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.isSelected[indexPath.section][indexPath.row] = !viewModel.isSelected[indexPath.section][indexPath.row]
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headersArray[section]
    }
}



import UIKit

class SupportViewModel: ViewModel {
    
    var isSelected = [
            [false],
            
            [false,
             false,
             false,
             false,
             false],
            
            [false,
             false,
             false]
    ]
    
    let headersArray = ["Dear Sir, Dear Ms, Dear Customer",
                        "How do I become the owner of German real estate?",
                        "What if I am already owner?"]
    
    let supportTexts =
        [
            "Welcome to Rendimmo and congratulations on your decision to invest in real estate. You naturally also realize that acquiring and certainly managing real estate does not always go smoothly. That is why Rendimmo has developed a concept of \"investing without worry\". Becoming carefree owner starts with this brochure that will give you insight into the actual course of your property purchase, as well as what it means to become / be owner. We have developed a procedure that is automated as much as possible and that you can follow in your personal electronic client file.",
            "As in Belgium, you will receive various letters from the German administration. Because Rendimmo wants to fully relieve you, we ask you not to handle these letters in person, but to upload them to your personal account at SAAD intranet - of which you have received the necessary information when booking - and to inform us of this by mail admin @ rendimmo .be. We will follow these documents further for you and keep you informed."
        ]
    
    let supportQuestions =
        [
            ["Welcome to Rendimmo and congratulations"],
            
            ["Reservation",
             "Manage personal SAAD account",
             "The notarial Deed and its Signature",
             "Management and rental guarantee contract",
             "Payment of the purchased property"],
            
            ["Rent Payment Confirmation / schedule",
             "What to do when german administrative and tax documents are received?",
             "Mandate for tax accountant"]
    ]
    
}

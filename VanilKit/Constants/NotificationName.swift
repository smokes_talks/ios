import Foundation

public enum NotificationName {
    public static let appEnterInForeground = NSNotification.Name("appEnterInForeground")
}

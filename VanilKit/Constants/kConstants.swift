import UIKit

public let kHandshakesToken = "HandshakesToken"
public let kRefreshToken = "HandshakesRefreshToken"
public let kTokenExpires = "HandshakesTokenExpires"
public let appID = "10241024"
public let deviceToken = UIDevice.current.identifierForVendor?.uuidString.description

public func logout() {
    UserDefaults.standard.set(nil, forKey: kTokenExpires)
    UserDefaults.standard.set(nil, forKey: kRefreshToken)
    UserDefaults.standard.set(nil, forKey: kHandshakesToken)
}

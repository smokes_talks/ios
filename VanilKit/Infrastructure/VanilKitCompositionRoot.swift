import Foundation

public class VanilKitCompositionRoot {

    public static var sharedInstance: VanilKitCompositionRoot = VanilKitCompositionRoot()

    private init() {}

    public lazy var resolveGetBestSearchesProfiles: GetBestSearchesProfiles = {
        return GetBestSearchesProfilesImpl.init()
    }()
    
    public lazy var resolveProfileDataStore: ProfileDataStore = {
        return DiskProfileDatastore.init()
    }()
    
    public lazy var resolveGetAppVersionAndCheckIt: GetAppVersionAndCheckIt = {
        return GetAppVersionAndCheckItImpl.init()
    }()
    
    public lazy var resolveProjectAPIManager: ProjectAPIManager = {
        return ProjectAPIManagerImpl.init()
    }()
    
    public var resolveAuthenticationAPIManager: AuthenticationAPIManager = {
        return AuthenticationAPIManagerImpl.init()
    }()
    
    public lazy var resolveCartDataStore: CartDataStore = {
        return CartDataStoreImpl.init()
    }()
}

import Foundation
import PromiseKit

public class FileUtils {
    public static let defaultDirectoryName = "Files"
    public static let appGroup = "VanilKit"

    public class var folderNamePathURL: URL? {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: appGroup)?.appendingPathComponent(defaultDirectoryName)
    }

    public class func getFolderName() -> String? {
        return createFolder(at: folderNamePathURL)?.path
    }

    public class func filePathWithMP4Extension(_ filename: String) -> URL? {
        if let filePath = FileUtils.filePath(filename) {
            return URL(fileURLWithPath: filePath, isDirectory: false).appendingPathExtension("mp4")
        }
        return nil
    }

    public class func filePath(_ fileName: String) -> String? {
        return filePath(forUrl: folderNamePathURL, fileName: fileName)
    }

    public class func publicFilePath(_ fileName: String) -> String? {
        return filePath(forUrl: publicFolderUrl, fileName: fileName)
    }

    public class func copy(at originUrl: URL, to destinationUrl: URL) -> Bool {
        let fileManager = FileManager.default
        do {
            try fileManager.copyItem(atPath: originUrl.appendingPathExtension("mp4").path, toPath: destinationUrl.appendingPathExtension("mp4").path)
            return true
        } catch {
            print("Unable to copy file from \(originUrl) to \(destinationUrl): \(error.localizedDescription)")
            return false
        }
    }

    public class func get(from url: URL) -> [String]? {
        let fileManager = FileManager.default
        do {
            let urls = try fileManager.contentsOfDirectory(atPath: url.absoluteString)
            return urls
        } catch {
            print("Unable to get files from \(url): \(error.localizedDescription)")
            return nil
        }
    }

    public class func save(data: Data, atPath path: String?) -> String? {
        guard let path = path else { return nil }

        if !FileManager.default.fileExists(atPath: path) {
            FileManager.default.createFile(atPath: path, contents: data, attributes: nil)
            print("File at path: \(path) added")
        }
        return path
    }

    public class func deleteFolder() -> Promise<Bool> {
        if let documentDirectoryPath = folderNamePathURL?.path {
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: documentDirectoryPath) {
                do {
                    print("Removing folder at path: \(documentDirectoryPath)")
                    try fileManager.removeItem(atPath: documentDirectoryPath)
                    try fileManager.removeItem(atPath: publicFolderName)
                    return Promise.value(true)
                } catch {
                    print("Error deleting TestRecords folders in documents dir: \(error)")
                    return Promise(error: error)
                }
            } else {
                print("There is no folder to erase.")
                return Promise.value(false)
            }
        } else {
            print("Seems to be a problem with deleting the test folder.")
            return Promise.value(false)
        }
    }

    public class func deleteFile(atPath path: String?) -> Promise<Bool> {
        guard let path = path else {
            return Promise.value(false)
        }

        return Promise { resolver in
            if FileManager.default.fileExists(atPath: path) {
                do {
                    try FileManager.default.removeItem(atPath: path)
                    print("File at path: \(path) deleted")
                    resolver.fulfill(true)
                } catch {
                    resolver.reject(error)
                }
            } else {
                resolver.fulfill(false)
            }
        }
    }

    public class func deleteFileSync(atPath path: String?) {
        guard let path = path else { return }
        if FileManager.default.fileExists(atPath: path) {
            do {
                try FileManager.default.removeItem(atPath: path)
                print("File at path: \(path) deleted")
            } catch {
                print("File at path: \(path) cannot be deleted")
            }
        }
    }

    public class var publicFolderUrl: URL? {
        return URL(string: publicFolderName)
    }

    public class var publicFolderName: String {
        createPublicFolder()
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).sorted()
        let documentsDirectory = paths[0] as String
        return "\(documentsDirectory)/\(defaultDirectoryName)"
    }

    public class func createFolder(at url: URL?) -> URL? {
        if let documentDirectoryPath = url?.path {
            createFolder(path: documentDirectoryPath)
        }
        return url
    }

    public class func createPublicFolder() {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).sorted().first else { return }
        createFolder(path: path.appending("/\(defaultDirectoryName)"))
    }

    public class func createFolder(path: String) {
        let fileManager = FileManager.default
        if !fileManager.fileExists(atPath: path) {
            do {
                try fileManager.createDirectory(atPath: path,
                                                withIntermediateDirectories: false,
                                                attributes: nil)
            } catch {
                print("Error creating \(path) folder: \(error)")
            }
        }
    }

    public class func filePath(forUrl url: URL?, fileName: String) -> String? {
        let folderURL = createFolder(at: url)
        if let filePath = folderURL?.appendingPathComponent(fileName).path {
//            if FileManager.default.fileExists(atPath: filePath) {
//                try? FileManager.default.removeItem(atPath: filePath)
//            }
            return filePath
        }
        return nil
    }
}

import Foundation

public struct ProfileModel: Codable {
    public var title: String
    public var fullName: String
    public var imageURL: URL
    public var phoneNumber: String
    public var skills: [String]
    public var description: String
    public var color: ColorModel?
    public var telegramURL: String?
    public var whatsappURL: String?
}

import Foundation

public struct CartDataStoreFoodModel {
    let foodID: String
    let count: Int
    let indexPath: Int
}

import Foundation

public class File {
    public let data: Data
    public var path: String?
    public let name: String?

    init(_ data: Data, _ path: String?, _ name: String?) {
        self.data = data
        self.path = path
        self.name = name
    }
}

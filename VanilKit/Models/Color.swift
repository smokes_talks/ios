import UIKit

public struct ColorModel: Codable {
    
    public var red: CGFloat
    public var blue: CGFloat
    public var green: CGFloat
    public var alpha: CGFloat
    
    var uiColor: UIColor {
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    
}

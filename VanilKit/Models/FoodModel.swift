import Foundation

public struct FoodModel: Codable {
    
    let id: String
    let price: Double
    let title: String
    let subTitle: String?
    let weight: Double?
    let image: String?
    
}

import Foundation
import Alamofire

public protocol GetAppVersionAndCheckIt {
    func check(completionHandler: @escaping (String?, String?, String?) -> Void)
}

class GetAppVersionAndCheckItImpl: GetAppVersionAndCheckIt {
    
    private let appVersion: String
    private var bundleID: String? {
        get {
            return Bundle.main.bundleIdentifier
        }
    }
    
    public init() {
        self.appVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as? String ?? "0.0.0"
    }
    
    public func check(completionHandler: @escaping (String?, String?, String?) -> Void) {
        guard let bundleID = bundleID else { return }
        Alamofire.request("http://itunes.apple.com/lookup?bundleId=\(bundleID)").response(completionHandler: { response in
            print(response)
        })
    }
    
}

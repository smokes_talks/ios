import Foundation

public struct PopularSearchModel: Codable {
    public let title: String
    public let candidates: [ProfileModel]
}

import Foundation

public protocol GetBestSearchesProfiles {
    func get(completion: ([PopularSearchModel]?, Error?)->())
}

public class GetBestSearchesProfilesImpl: GetBestSearchesProfiles {
    
    let popularSearchesModel: [PopularSearchModel] = [PopularSearchModel(title: "Best 10 UI/UX Designers",
                                                                         candidates: RandomPorfile.randomProfiles(count: 10)),
                                                      PopularSearchModel(title: "Best 5 iOS Developers",
                                                                         candidates: RandomPorfile.randomProfiles(count: 5)),
                                                      PopularSearchModel(title: "Best House Cleaners in Odessa",
                                                                         candidates: RandomPorfile.randomProfiles(count: 5)),
                                                      PopularSearchModel(title: "Best Дедушки с котороыми можно выпить пиво",
                                                                         candidates: RandomPorfile.randomProfiles(count: 5)),
                                                      PopularSearchModel(title: "5 лучших мамочки - кухарки",
                                                                         candidates: RandomPorfile.randomProfiles(count: 5)),
                                                      PopularSearchModel(title: "Best 80 UI/UX Designers",
                                                                         candidates: RandomPorfile.randomProfiles(count: 5)),
                                                      PopularSearchModel(title: "Best 5 UI/UX Designers",
                                                                         candidates: RandomPorfile.randomProfiles(count: 5))]
    
    public func get(completion: ([PopularSearchModel]?, Error?) -> ()) {
        completion(popularSearchesModel, nil)
    }
    
}

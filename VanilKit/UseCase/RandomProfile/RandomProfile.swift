import UIKit


public class RandomPorfile {
    
    private static let randomFullNames: [String] = ["Фомичёв Радислав",
                                 "Ковальчук Жерар",
                                 "Васильев Осип",
                                 "Щербаков Геннадий",
                                 "Карпенко-Карый Юлий",
                                 "Кобзар Диана",
                                 "Ланова Люся",
                                 "Иванова Яна",
                                 "Ефремова Валентина",
                                 "Фролова Диана",
                                 "Гуляева Цилла",
                                 "Дубченко Регина",
                                 "Жданова Таисия",
                                 "Кулакова Дина"]
    
    private static let randomPhoneNumbers: [String] = ["0935463712",
                                        "0936341752",
                                        "0936371524",
                                        "0931463572",
                                        "0936754132",
                                        "0935271436",
                                        "0934615723",
                                        "0932743156",
                                        "0935124367",
                                        "0931624375",
                                        "0936425173",
                                        "0933146257",
                                        "0931475623",
                                        "0932431756",
                                        "0936754132",
                                        "0935623174",
                                        "0937531246",
                                        "0931637524",
                                        "0935127436",
                                        "0931357462"]
    
    private static let randomSkills: [String] = ["Lockpicking",
                                  "Homebrewing Beer",
                                  "Cooking and Grilling",
                                  "Knife Making",
                                  "Fantasy Sports",
                                  "Geocaching",
                                  "Archery",
                                  "Homebrewing Beer",
                                  "Meditation",
                                  "Metalworking",
                                  "Guitar",
                                  "Playing Tennis",
                                  "Mosaic Art",
                                  "Learning A Language",
                                  "Classic Car Restoration",
                                  "Calligraphy",
                                  "Interior design",
                                  "Traditional Sports",
                                  "Volunteering",
                                  "Meditation"]
    
    private static let randomUsernames: [String] = ["grammarianbudweiser",
                                                    "midwifenissan",
                                                    "fitterlego",
                                                    "girlguideaudi",
                                                    "employeegoogle",
                                                    "judgeamazon",
                                                    "cookvolkswagon",
                                                    "arsonistburberry",
                                                    "examinerdisney",
                                                    "barmanchevrolet",
                                                    "millwrightallianz",
                                                    "advisorprada",
                                                    "nunford",
                                                    "magistratedanone",
                                                    "boyscoutsmercedes",
                                                    "astronomerlexus",
                                                    "craftsmansamsung",
                                                    "cameramanshell",
                                                    "trainerhyundai",
                                                    "researcherlancome",
                                                    "producerstarbucks",
                                                    "tileradidas",
                                                    "senatorboeing",
                                                    "canonsantander",
                                                    "programmerchanel",
                                                    "announcerphilips",
                                                    "gamblerfacebook",
                                                    "analystchase",
                                                    "churchgoermalboro",
                                                    "cobblerintel"]
    
        private static let randomColors: [UIColor] = [UIColor(red:0.08, green:0.08, blue:0.13, alpha:1.00),
                                         UIColor(red:0.89, green:0.89, blue:0.89, alpha:1.00),
                                         UIColor(red:0.75, green:0.32, blue:0.95, alpha:1.00),
                                         UIColor(red:0.41, green:0.47, blue:0.97, alpha:1.00),
                                         UIColor(red:1.00, green:0.81, blue:0.36, alpha:1.00),
                                         UIColor(red:0.95, green:0.60, blue:0.29, alpha:1.00),
                                         UIColor(red:0.00, green:0.52, blue:0.96, alpha:1.00),
                                         UIColor(red:0.00, green:0.77, blue:0.55, alpha:1.00),
                                         UIColor(red:1.00, green:0.39, blue:0.49, alpha:1.00)]
    
    public static func randomProfile() -> ProfileModel {
        ProfileModel(title: "iOS Developer",
                     fullName: randomFullNames.randomElement() ?? "Фомичёв Радислав",
                     imageURL: URL(string: "https://img.freepik.com/free-vector/businessman-profile-cartoon_18591-58479.jpg?size=338&ext=jpg")!,
                     phoneNumber: randomPhoneNumbers.randomElement() ?? "0936341752",
                     skills: randomSkills.shuffled(),
                     description: "ЭЭ, у меня двое детей",
                     telegramURL: randomUsernames.randomElement() ?? "magistratedanone",
                     whatsappURL: randomUsernames.randomElement() ?? "magistratedanone")
    }
    
    public static func randomProfiles(count: Int) -> [ProfileModel] {
    
        var profiles: [ProfileModel] = [ProfileModel]()
        for _ in 0..<count {
            profiles.append(randomProfile())
        }
        return profiles
    }
    
}

import Foundation

public enum NetworkError: Error {
    case apiError(error: Error)
    case genericError(error: GenericNetworkError)
    case unknown(error: Error)
}

public enum GenericNetworkError: Error {
    case underlyingNetworkError(error: Error)
    case parsingResponseError(error: Error)
}

public struct APIError<T>: Error {
    let statusCode: Int
    let errorData: T
}

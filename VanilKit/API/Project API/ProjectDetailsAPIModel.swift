import Foundation

public struct ProjectDetailsAPIModel: Codable {
    public var en: Language?
    public var nl: Language?
    public var de: Language?
    public var fr: Language?
    
    enum CodingKeys: String, CodingKey {
        case en, nl, de, fr
    }
}

public struct Language: Codable {
    public var columns: [Column]?
    public var rows: [Row]?
    
    enum CodingKeys: String, CodingKey {
        case columns, rows
    }
}

public struct Column: Codable {
    public var key: String?
    public var label: String?
    
    enum CodingKeys: String, CodingKey {
        case key, label
    }
}

public struct Row: Codable {
    public var id: Int?
    public var values: Value?
    
    enum CodingKeys: String, CodingKey {
        case id, values
    }
}

public struct Value: Codable {
    public var col0: String?
    public var col1: String?
    
    enum CodingKeys: String, CodingKey {
        case col0, col1
    }
}

import Foundation
import Alamofire

public protocol ProjectAPIManager {
    func fetchProjects(completionHandler: @escaping ([ProjectAPIModel]?) -> Void)
}

public class ProjectAPIManagerImpl: ProjectAPIManager {
    
    private let projectsUrlStr = "http://api.handshakes.thirty3.tools/v1/projects?per-page=50&expand=apartmentsSum%2CtotalApartments&filters=%7B%22status%22%3A1%2C%22on_dashboard%22%3A1%7D"
    
    public func fetchProjects(completionHandler: @escaping ([ProjectAPIModel]?) -> Void) {
        guard let projectsUrl = URL(string: projectsUrlStr) else { return }
        Alamofire
            .request(projectsUrl,
                     method: .get,
                     headers: headers)
            .response { response in
            if let data = response.data {
                do {
                    let result = try JSONDecoder().decode([ProjectAPIModel].self, from: data)
                    completionHandler(result)
                } catch {
                    print(error.localizedDescription)
                }
            }
        }
    }
}

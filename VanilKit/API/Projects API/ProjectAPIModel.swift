import Foundation

// MARK: - ProjectAPIModel
public struct ProjectAPIModel: Codable {
    public var id: Int?
    public var name, details: String?
    public var status: Int?
    public var createdAt, updatedAt: String?
    public var createdBy, updatedBy: Int?
    public var address, more, equipment: String?
    public var youtubeURL: String?
    public var isPublic: Int?
    public var other: String?
    public var zip, city: String?
    public var latitude, longitude: Double?
    public var transferTax: String?
    public var onDashboard: Int?
    public var onDashboardSort: Int?
    public var logoImageID: Int?
    public var rentPerM2, yield: Double?
    public var oguloURL, preziURL: String?
    public var droneURL: String?
    public var projectDetails: ProjectDetailsAPIModel?
    public var emailImageID: Int?
    public var maxRebatePercent: Double?
    public var energyCertificate: String?
    public var yieldThreshold, rentPerM2Threshold: String?
    public var allowedBankAccess: Int?
    public var pdfStructure: String?
    public var country: String?
    public var moreDe, equipmentDe, otherDe, equipmentNl: String?
    public var equipmentFr, moreNl, moreFr, otherNl: String?
    public var otherFr: String?
    public var logo: Logo?
    public var reservedApartments, soldApartments, totalApartments: Int?
    public var creator: Creator?
    public var lowerArea, higherArea: Double?
    public var lowerPrice, higherPrice: Double?
    public var totalPrice, totalRent, lowerRent, higherRent: Double?
    public var yieldCalculated, rentPerM2Calculated: String?
    public var apartmentsSum: ApartmentsSum?
    
    enum CodingKeys: String, CodingKey {
        case id, name, details, status
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case createdBy = "created_by"
        case updatedBy = "updated_by"
        case address, more, equipment
        case youtubeURL = "youtube_url"
        case isPublic = "is_public"
        case other, zip, city, latitude, longitude
        case transferTax = "transfer_tax"
        case onDashboard = "on_dashboard"
        case onDashboardSort = "on_dashboard_sort"
        case logoImageID = "logo_image_id"
        case rentPerM2 = "rent_per_m2"
        case yield
        case oguloURL = "ogulo_url"
        case preziURL = "prezi_url"
        case droneURL = "drone_url"
        case projectDetails = "project_details"
        case emailImageID = "email_image_id"
        case maxRebatePercent = "max_rebate_percent"
        case energyCertificate = "energy_certificate"
        case yieldThreshold = "yield_threshold"
        case rentPerM2Threshold = "rent_per_m2_threshold"
        case allowedBankAccess = "allowed_bank_access"
        case pdfStructure = "pdf_structure"
        case country
        case moreDe = "more_de"
        case equipmentDe = "equipment_de"
        case otherDe = "other_de"
        case equipmentNl = "equipment_nl"
        case equipmentFr = "equipment_fr"
        case moreNl = "more_nl"
        case moreFr = "more_fr"
        case otherNl = "other_nl"
        case otherFr = "other_fr"
        case logo, reservedApartments, soldApartments, totalApartments, creator, lowerArea, higherArea, lowerPrice, higherPrice, totalPrice, totalRent, lowerRent, higherRent
        case yieldCalculated = "yield_calculated"
        case rentPerM2Calculated = "rent_per_m2_calculated"
        case apartmentsSum
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(String.self, forKey: .lowerPrice) {
            lowerPrice = Double(value)
        } else {
            lowerPrice = nil
        }
        if let value = try? container.decode(String.self, forKey: .higherPrice) {
            higherPrice = Double(value)
        } else {
            higherPrice = nil
        }
        if let value = try? container.decode(Int.self, forKey: .id) {
            id = value
        } else {
            id = nil
        }
        if let value = try? container.decode(String.self, forKey: .name) {
            name = value
        } else {
            name = nil
        }
        if let value = try? container.decode(String.self, forKey: .details) {
            details = value
        } else {
            details = nil
        }
        if let value = try? container.decode(Int.self, forKey: .status) {
            status = value
        } else {
            status = nil
        }
        if let value = try? container.decode(String.self, forKey: .createdAt) {
            createdAt = value
        } else {
            createdAt = nil
        }
        if let value = try? container.decode(String.self, forKey: .updatedAt) {
            updatedAt = value
        } else {
            updatedAt = nil
        }
        if let value = try? container.decode(Int.self, forKey: .createdBy) {
            createdBy = value
        } else {
            createdBy = nil
        }
        if let value = try? container.decode(Int.self, forKey: .updatedBy) {
            updatedBy = value
        } else {
            updatedBy = nil
        }
        if let value = try? container.decode(String.self, forKey: .address) {
            address = value
        } else {
            address = nil
        }
        if let value = try? container.decode(String.self, forKey: .more) {
            more = value
        } else {
            more = nil
        }
        if let value = try? container.decode(String.self, forKey: .equipment) {
            equipment = value
        } else {
            equipment = nil
        }
        if let value = try? container.decode(String.self, forKey: .youtubeURL) {
            youtubeURL = value
        } else {
            youtubeURL = nil
        }
        if let value = try? container.decode(Int.self, forKey: .isPublic) {
            isPublic = value
        } else {
            isPublic = nil
        }
        if let value = try? container.decode(String.self, forKey: .other) {
            other = value
        } else {
            other = nil
        }
        if let value = try? container.decode(String.self, forKey: .zip) {
            zip = value
        } else {
            zip = nil
        }
        if let value = try? container.decode(String.self, forKey: .city) {
            city = value
        } else {
            city = nil
        }
        if let value = try? container.decode(String.self, forKey: .latitude) {
            latitude = Double(value)
        } else {
            latitude = nil
        }
        if let value = try? container.decode(String.self, forKey: .longitude) {
            longitude = Double(value)
        } else {
            longitude = nil
        }
        if let value = try? container.decode(String.self, forKey: .transferTax) {
            transferTax = value
        } else {
            transferTax = nil
        }
        if let value = try? container.decode(Int.self, forKey: .onDashboard) {
            onDashboard = value
        } else {
            onDashboard = nil
        }
        if let value = try? container.decode(Int.self, forKey: .onDashboardSort) {
            onDashboardSort = value
        } else {
            onDashboardSort = nil
        }
        if let value = try? container.decode(Int.self, forKey: .logoImageID) {
            logoImageID = value
        } else {
            logoImageID = nil
        }
        if let value = try? container.decode(String.self, forKey: .rentPerM2) {
            rentPerM2 = Double(value)
        } else {
            rentPerM2 = nil
        }
        if let value = try? container.decode(String.self, forKey: .yield) {
            yield = Double(value)
        } else {
            yield = nil
        }
        if let value = try? container.decode(String.self, forKey: .oguloURL) {
            oguloURL = value
        } else {
            oguloURL = nil
        }
        if let value = try? container.decode(String.self, forKey: .preziURL) {
            preziURL = value
        } else {
            preziURL = nil
        }
        if let value = try? container.decode(String.self, forKey: .droneURL) {
            droneURL = value
        } else {
            droneURL = nil
        }
        // Convert response string into JSON
        if let value = try? container.decode(String.self, forKey: .projectDetails) {
            if let data = value.data(using: .utf8) {
                do {
                    let jsonDetails = try JSONDecoder().decode(ProjectDetailsAPIModel.self, from: data)
                    projectDetails = jsonDetails
                } catch {
                    print(error.localizedDescription)
                }
            }
        } else {
            projectDetails = nil
        }
        if let value = try? container.decode(Int.self, forKey: .emailImageID) {
            emailImageID = value
        } else {
            emailImageID = nil
        }
        if let value = try? container.decode(Double.self, forKey: .maxRebatePercent) {
            maxRebatePercent = value
        } else {
            maxRebatePercent = nil
        }
        if let value = try? container.decode(String.self, forKey: .energyCertificate) {
            energyCertificate = value
        } else {
            energyCertificate = nil
        }
        if let value = try? container.decode(String.self, forKey: .yieldThreshold) {
            yieldThreshold = value
        } else {
            yieldThreshold = nil
        }
        if let value = try? container.decode(String.self, forKey: .rentPerM2Threshold) {
            rentPerM2Threshold = value
        } else {
            rentPerM2Threshold = nil
        }
        if let value = try? container.decode(Int.self, forKey: .allowedBankAccess) {
            allowedBankAccess = value
        } else {
            allowedBankAccess = nil
        }
        if let value = try? container.decode(String.self, forKey: .pdfStructure) {
            pdfStructure = value
        } else {
            pdfStructure = nil
        }
        if let value = try? container.decode(String.self, forKey: .country) {
            country = value
        } else {
            country = nil
        }
        if let value = try? container.decode(String.self, forKey: .moreDe) {
            moreDe = value
        } else {
            moreDe = nil
        }
        if let value = try? container.decode(String.self, forKey: .equipmentDe) {
            equipmentDe = value
        } else {
            equipmentDe = nil
        }
        if let value = try? container.decode(String.self, forKey: .otherDe) {
            otherDe = value
        } else {
            otherDe = nil
        }
        if let value = try? container.decode(String.self, forKey: .equipmentNl) {
            equipmentNl = value
        } else {
            equipmentNl = nil
        }
        if let value = try? container.decode(String.self, forKey: .equipmentFr) {
            equipmentFr = value
        } else {
            equipmentFr = nil
        }
        if let value = try? container.decode(String.self, forKey: .moreNl) {
            moreNl = value
        } else {
            moreNl = nil
        }
        if let value = try? container.decode(String.self, forKey: .moreFr) {
            moreFr = value
        } else {
            moreFr = nil
        }
        if let value = try? container.decode(String.self, forKey: .otherNl) {
            otherNl = value
        } else {
            otherNl = nil
        }
        if let value = try? container.decode(String.self, forKey: .otherFr) {
            otherFr = value
        } else {
            otherFr = nil
        }
        if let value = try? container.decode(Logo.self, forKey: .logo) {
            logo = value
        } else {
            logo = nil
        }
        if let value = try? container.decode(String.self, forKey: .reservedApartments) {
            reservedApartments = Int(value)
        } else {
            reservedApartments = nil
        }
        if let value = try? container.decode(String.self, forKey: .soldApartments) {
            soldApartments = Int(value)
        } else {
            soldApartments = nil
        }
        if let value = try? container.decode(String.self, forKey: .totalApartments) {
            totalApartments = Int(value)
        } else {
            totalApartments = nil
        }
        if let value = try? container.decode(Creator.self, forKey: .creator) {
            creator = value
        } else {
            creator = nil
        }
        if let value = try? container.decode(String.self, forKey: .lowerArea) {
            lowerArea = Double(value)
        } else {
            lowerArea = nil
        }
        if let value = try? container.decode(String.self, forKey: .higherArea) {
            higherArea = Double(value)
        } else {
            higherArea = nil
        }
        if let value = try? container.decode(Double.self, forKey: .totalPrice) {
            totalPrice = value
        } else {
            totalPrice = nil
        }
        if let value = try? container.decode(Double.self, forKey: .totalRent) {
            totalRent = value
        } else {
            totalRent = nil
        }
        if let value = try? container.decode(Double.self, forKey: .lowerRent) {
            lowerRent = value
        } else {
            lowerRent = nil
        }
        if let value = try? container.decode(Double.self, forKey: .higherRent) {
            higherRent = value
        } else {
            higherRent = nil
        }
        if let value = try? container.decode(String.self, forKey: .yieldCalculated) {
            yieldCalculated = value
        } else {
            yieldCalculated = nil
        }
        if let value = try? container.decode(String.self, forKey: .rentPerM2Calculated) {
            rentPerM2Calculated = value
        } else {
            rentPerM2Calculated = nil
        }
        if let value = try? container.decode(ApartmentsSum.self, forKey: .apartmentsSum) {
            apartmentsSum = value
        } else {
            apartmentsSum = nil
        }
    }
}

// MARK: - ApartmentsSum
public struct ApartmentsSum: Codable {
    public let available, all: Double?
}

// MARK: - Creator
public struct Creator: Codable {
    public let id: Int?
    public let username: String?
    public let profile: Profile?
    public let agency: String?
    public let email: String?
    public let userAgencies: [String]?
    public let agentID: String?
    
    enum CodingKeys: String, CodingKey {
        case id, username, profile, agency, email, userAgencies
        case agentID = "agent_id"
    }
}

// MARK: - Profile
public struct Profile: Codable {
    public let userID: Int?
    public let location, timezone: String?
    public let firstName: String?
    public let lastName: String?
    public let address, country, state, zip: String?
    public let phone: String?
    public let gender: String?
    public let dob, city: String?
    public let logoImageID: Int?
    public let bankName, bankAddress, bankIban, bankBic: String?
    public let bankName2, bankAddress2, bankIban2, bankBic2: String?
    public let language: String?
    public let confirmationEnabled: Int?
    public let leadAgency, leadAgent: String?
    public let phoneSecond, phoneMobile, fax: String?
    public let other: String?
    public let targetSales: String?
    public let notificationSettings: String?
    public let idNumber: String?
    public let directorName: String?
    public let logo: Logo?
    
    enum CodingKeys: String, CodingKey {
        case userID = "user_id"
        case location, timezone
        case firstName = "first_name"
        case lastName = "last_name"
        case address, country, state, zip, phone, gender, dob, city
        case logoImageID = "logo_image_id"
        case bankName = "bank_name"
        case bankAddress = "bank_address"
        case bankIban = "bank_iban"
        case bankBic = "bank_bic"
        case bankName2 = "bank_name2"
        case bankAddress2 = "bank_address2"
        case bankIban2 = "bank_iban2"
        case bankBic2 = "bank_bic2"
        case language
        case confirmationEnabled = "confirmation_enabled"
        case leadAgency = "lead_agency"
        case leadAgent = "lead_agent"
        case phoneSecond = "phone_second"
        case phoneMobile = "phone_mobile"
        case fax, other
        case targetSales = "target_sales"
        case notificationSettings = "notification_settings"
        case idNumber = "id_number"
        case directorName = "director_name"
        case logo
    }
}

// MARK: - Logo
public struct Logo: Codable {
    public let id: Int?
    public let fullscale, preview: String?
    public let width, height, isPublic: Int?
    
    enum CodingKeys: String, CodingKey {
        case id, fullscale, preview, width, height
        case isPublic = "is_public"
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        if let value = try? container.decode(Int.self, forKey: .id) {
            id = value
        } else {
            id = nil
        }
        if let value = try? container.decode(String.self, forKey: .fullscale) {
            fullscale = "https:" + value
        } else {
            fullscale = nil
        }
        if let value = try? container.decode(String.self, forKey: .preview) {
            preview = "https:" + value
        } else {
            preview = nil
        }
        if let value = try? container.decode(Int.self, forKey: .width) {
            width = value
        } else {
            width = nil
        }
        if let value = try? container.decode(Int.self, forKey: .height) {
            height = value
        } else {
            height = nil
        }
        if let value = try? container.decode(Int.self, forKey: .isPublic) {
            isPublic = value
        } else {
            isPublic = nil
        }
    }
}



import Alamofire
import Foundation
import PromiseKit

public protocol ResultAPIEndpoint {
    func checkAppVersion(appVersion: String, completionHandler: @escaping (String?, String?, String?) -> Void)
//    func sendDeviceInfo(id: String, completionHandler: (() -> Void)?)
//    func getConfig() -> Promise<ResultConfigAPIModel?>
//    func getInfo(id: String) -> Promise<ResultInfoAPIModel?>
//    func updateStatus(id: String, status: ResultInfo.Status) -> Promise<ResultStatusUpdateAPIModel?>
//    func login(email: String, pass: String) -> Promise<ResultAuthLoginAPIModel?>
//    func profile() -> Promise<ProfileAPIModel?>
}

extension ResultAPIEndpoint {
    
}

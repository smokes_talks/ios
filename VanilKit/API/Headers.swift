import Foundation

public var headers: [String: String] {
    get {
        return [
            "Content-Type": "application/json;charset=utf-8",
            "Authorization": "Bearer \(token ?? "Guest")",
            "Host": "dev-api.handshakes.com",
        ]
    }
}


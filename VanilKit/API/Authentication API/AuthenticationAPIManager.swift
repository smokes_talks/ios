import Alamofire

private struct HostSettings {
    
    static let baseUrl = "http://api.handshakes.thirty3.tools/v1/"
    
}

public protocol AuthenticationAPIManager {
    func authenticateUser(
        username: String,
        andPassword password: String,
        completionHandler: @escaping (String?) -> Void,
        errorHandler: @escaping (Int) -> Void)
}

public class AuthenticationAPIManagerImpl: AuthenticationAPIManager {
    
    private var serverURL: String { return HostSettings.baseUrl }
    
    public func authenticateUser(username: String,
                              andPassword password: String,
                              completionHandler: @escaping (String?) -> Void,
                              errorHandler: @escaping (Int) -> Void) {
        let urlStr = serverURL
        let authenticationApiModel = AuthenticationAPIModel(
            username: username,
            password: password)
        Alamofire
            .request(
                "\(urlStr)users/login",
                method: .post,
                parameters: authenticationApiModel.dictionary,
                encoding: JSONEncoding.default,
                headers: headers)
            .responseJSON { response in
                if let data = response.data {
                    do {
                        let statusCode =
                            try JSONSerialization.jsonObject(with: data,
                                                             options: []) as? [String: Any]
                        if let status =  statusCode!["status"] as? Int {
                            errorHandler(status)
                        }
                    } catch {
                        print(error.localizedDescription)
                    }
                    do {
                        let result = try JSONDecoder().decode(AuthenticationAPIResponseModel.self,
                                                              from: data)
                        guard let token = result.token else { return }
                        completionHandler(token)
                    } catch let error {
                        print(error.localizedDescription)
                    }
                }
        }
    }
    
}

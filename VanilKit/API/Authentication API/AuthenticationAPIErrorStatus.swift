import Foundation

public struct AuthenticationAPIErrorStatus {
    
    public static func messageForUserByStatusCode(statusCode: Int) -> (title: String, message:String)? {
        switch statusCode {
        case 400:
            return ("Wrong login or password", "Enter correct login/email and password")
        case 404:
            return ("Wrong login or password", "Enter correct login/email and password")
        default:
            return nil
        }
    }
}

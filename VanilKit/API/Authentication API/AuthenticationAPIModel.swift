import Foundation

public struct AuthenticationAPIModel: Codable {
    
    public let username: String
    public let password: String
    
    enum CodingKeys: String, CodingKey {
        case username = "username"
        case password = "password"
    }
    
}

public struct AuthenticationAPIResponseModel: Codable {
    
    public let id: Int?
    public let token: String?
    public let displayName: String?
    public let language: String?
    public let acl: Acl?
    public let accountAgencies: [AccountAgency]?
    public let confirmationEnabled: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case token = "token"
        case displayName = "displayName"
        case language = "language"
        case acl = "acl"
        case accountAgencies = "accountAgencies"
        case confirmationEnabled = "confirmation_enabled"
    }
    
}

public struct Acl: Codable {
    
    let prospect: [String]?
    
    enum CodingKeys: String, CodingKey {
        case prospect = "prospect"
    }
    
}

public struct AccountAgency: Codable {
    
    public let agencyId: Int?
    public let role: String?
    public let agencyParentId: String?
    
    enum CodingKeys: String, CodingKey {
        case agencyId = "agency_id"
        case role = "role"
        case agencyParentId = "agnecy_parent_id"
    }
    
}

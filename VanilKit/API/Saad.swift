import Foundation

public var token: String? {
    get {
        return UserDefaults.standard.string(forKey: "accessToken") ?? nil
    }
}

public class VanilKit {
    public static func start(groupId: String, apiBasePath: String, videoBucketName: String) {
        handshakesVariables = ["group_id": groupId,
                               "api_base_path": apiBasePath,
                               "video_bucket_name": videoBucketName]
    }

    public static func start(groupId: String) {
        handshakesVariables = ["group_id": groupId]
    }

    static var handshakesVariables: [String: String]?

    static var appGroup: String? {
        return handshakesVariables?["group_id"]
    }
}

class Handshakes {
    static var apiEndpoint: String {
        return VanilKit.handshakesVariables!["api_base_path"] ?? "https://api-dev.handshakes.com"
    }

    static var bucketName: String {
        return VanilKit.handshakesVariables!["video_bucket_name"] ?? "handshakes-apps.dev"
    }
}

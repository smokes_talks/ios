import Foundation

public protocol CartDataStoreDelegate {
    func updatedStore(count: Int)
}

public protocol CartDataStore {
    var delegate: CartDataStoreDelegate? { get set }
    func put(_ indexPath: Int, foodID: String, count: Int)
    func remove(_ indexPath: Int, foodID: String)
    func get() -> [CartDataStoreFoodModel]?
}

class CartDataStoreImpl: CartDataStore {
    
    public static var sharedInstance: CartDataStore = CartDataStoreImpl()
    
    var delegate: CartDataStoreDelegate?
    var products: [CartDataStoreFoodModel]? = [CartDataStoreFoodModel]()
    
    func put(_ indexPath: Int, foodID: String, count: Int) {
        if count <= 0 {
            remove(indexPath, foodID: foodID);
        } else {
            let product = CartDataStoreFoodModel(foodID: foodID, count: count, indexPath: indexPath)
            self.products?.append(product)
        }
        self.delegate?.updatedStore(count: self.products?.count ?? 0)
    }
    
    func remove(_ indexPath: Int, foodID: String) {
        self.products?.removeAll(where: { food -> Bool in
            if food.foodID == foodID && food.indexPath == indexPath { return true } else { return false }
        })
        self.delegate?.updatedStore(count: self.products?.count ?? 0)
    }
    
    func get() -> [CartDataStoreFoodModel]? {
        return products
    }
    
}

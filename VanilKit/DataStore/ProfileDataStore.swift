import Foundation

public protocol ProfileDataStore {
    func put(_ profile: ProfileModel?) -> ProfileModel?
    func get() -> ProfileModel?
    func logOut()
}

extension ProfileDataStore {
    public func update(username: String) -> ProfileModel? {
//        if let profile = get() {
//            return put(ProfileModel(username: username, number: profile.number, details: profile.details))
//        }
        return nil
    }

    public func update(number: String) -> ProfileModel? {
//        if let profile = get() {
//            return put(ProfileModel(username: profile.username, number: number, details: profile.details))
//        }
        return nil
    }

    public func update(details: String) -> ProfileModel? {
//        if let profile = get() {
//            return put(ProfileModel(username: profile.username, number: profile.number, details: details))
//        }
        return nil
    }
}

public class DiskProfileDatastore: ProfileDataStore {
    private let storage: UserDefaults
    private let storeKey = "profile"

    public init() {
        guard let sharedDefaults = UserDefaults(suiteName: "Message.Store") else {
            print("There was an error obtaining the shared default.")
            storage = UserDefaults.standard
            return
        }
        storage = sharedDefaults
    }

    public func put(_ profile: ProfileModel?) -> ProfileModel? {
        guard let profile = profile else {
            storage.removeObject(forKey: storeKey)
            storage.synchronize()
            return nil
        }
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(profile) {
            storage.set(encoded, forKey: storeKey)
            storage.synchronize()
        }
        return profile
    }

    public func get() -> ProfileModel? {
        let decoder = JSONDecoder()
        if let storedData = storage.data(forKey: storeKey), let profile = try? decoder.decode(ProfileModel.self, from: storedData) {
            return profile
        }
        return nil
    }

    public func logOut() {
            storage.removeObject(forKey: storeKey)
            storage.synchronize()
    }
}
